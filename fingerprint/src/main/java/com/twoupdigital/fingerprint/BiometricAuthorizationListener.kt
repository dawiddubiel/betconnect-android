package com.twoupdigital.fingerprint

interface BiometricAuthorizationListener {
    fun onCredentialsDecrypted(login: String, password: String)
    fun onAuthorizationError()
}