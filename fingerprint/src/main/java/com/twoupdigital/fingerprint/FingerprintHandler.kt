package com.twoupdigital.fingerprint

import android.annotation.SuppressLint
import android.hardware.fingerprint.FingerprintManager.FINGERPRINT_ERROR_LOCKOUT
import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import androidx.annotation.RequiresApi
import androidx.core.hardware.fingerprint.FingerprintManagerCompat
import androidx.core.os.CancellationSignal
import java.io.IOException
import java.security.*
import java.security.cert.CertificateException
import java.security.spec.InvalidKeySpecException
import java.security.spec.MGF1ParameterSpec
import java.security.spec.X509EncodedKeySpec
import javax.crypto.BadPaddingException
import javax.crypto.Cipher
import javax.crypto.IllegalBlockSizeException
import javax.crypto.NoSuchPaddingException
import javax.crypto.spec.OAEPParameterSpec
import javax.crypto.spec.PSource

@SuppressLint("NewApi")
class FingerprintHandler(private val credentialsRepo: ICredentailsRero,
                         private val fingerprintManagerCompat: FingerprintManagerCompat,
                         private val onAuthError: (() -> Unit)?,
                         private val onCredentialsDecrypted: ((String, String) -> Unit)?) : FingerprintManagerCompat.AuthenticationCallback() {

    private val keyStore: KeyStore? by lazy { initKeyStore() }
    private val cipher: Cipher? by lazy { initCipher() }
    private val sKeyPairGenerator: KeyPairGenerator? by lazy { initKeyGenerator() }


    private val KEY_STORE = "AndroidKeyStore"
    private val KEY_ALIAS = "FINGERPRINT_KEY_PAIR_ALIAS"

    private val cancellationSignal: CancellationSignal = CancellationSignal()

    fun encryptString(rawString: String): String? {
        keyStore?.let { keyStore ->
            cipher?.let { cipher ->
                if (initKey() && initCipherMode(keyStore, cipher, Cipher.ENCRYPT_MODE)) {
                    val bytes = cipher.doFinal(rawString.toByteArray())
                    return Base64.encodeToString(bytes, Base64.NO_WRAP)
                }
            }
        }
        return null
    }

    private fun decryptString(string: String?, cipher: Cipher?): String? {
        string?.let {
            try {
                val bytes = Base64.decode(string, Base64.NO_WRAP)
                return cipher?.let { String(cipher.doFinal(bytes)) }
            } catch (exception: IllegalBlockSizeException) {
                exception.printStackTrace()
            } catch (exception: BadPaddingException) {
                exception.printStackTrace()
            }
        }
        return null
    }

    private fun initKeyStore(): KeyStore? {
        try {
            return KeyStore.getInstance("AndroidKeyStore").apply {
                this.load(null)
            }
        } catch (e: KeyStoreException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: CertificateException) {
            e.printStackTrace()
        }
        return null
    }

    private fun initCipher(): Cipher? {
        try {
            return Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding")
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchPaddingException) {
            e.printStackTrace()
        }
        return null
    }

    private fun initKey(): Boolean {
        try {
            return keyStore?.containsAlias(KEY_ALIAS) ?: false || generateNewKey()
        } catch (e: KeyStoreException) {
            e.printStackTrace()
        }
        return false
    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun generateNewKey(): Boolean {
        sKeyPairGenerator?.let {
            try {
                it.initialize(KeyGenParameterSpec.Builder(KEY_ALIAS,
                    KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                    .setDigests(KeyProperties.DIGEST_SHA256, KeyProperties.DIGEST_SHA512)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_OAEP)
                    .setUserAuthenticationRequired(true)
                    .build())
                it.generateKeyPair()
                return true
            } catch (e: InvalidAlgorithmParameterException) {
                e.printStackTrace()
            }
        }
        return false
    }


    private fun initKeyGenerator(): KeyPairGenerator? {
        try {
            return KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, KEY_STORE)
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchProviderException) {
            e.printStackTrace()
        }
        return null
    }

    private fun initCipherMode(sKeyStore: KeyStore, sCipher: Cipher, mode: Int): Boolean {
        try {
            sKeyStore.load(null)
            when (mode) {
                Cipher.ENCRYPT_MODE -> {
                    val key = sKeyStore.getCertificate(KEY_ALIAS).publicKey
                    val unrestricted = KeyFactory.getInstance(key.algorithm).generatePublic(X509EncodedKeySpec(key.encoded))
                    val spec = OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA1, PSource.PSpecified.DEFAULT)
                    sCipher.init(mode, unrestricted, spec)
                }

                Cipher.DECRYPT_MODE -> {
                    val privateKey = sKeyStore.getKey(KEY_ALIAS, null) as PrivateKey
                    sCipher.init(mode, privateKey)
                }
                else -> return false
            }
            return true
        } catch (e: KeyStoreException) {
            e.printStackTrace()
        } catch (e: CertificateException) {
            e.printStackTrace()
        } catch (e: UnrecoverableKeyException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: InvalidKeyException) {
            e.printStackTrace()
        } catch (e: InvalidAlgorithmParameterException) {
            e.printStackTrace()
        } catch (e: InvalidKeySpecException) {
            e.printStackTrace()
        }
        return false
    }

    private fun getCryptoObject(): FingerprintManagerCompat.CryptoObject? {
        keyStore?.let { keyStore ->
            cipher?.let { cipher ->
                return if (initKey() && initCipherMode(keyStore, cipher, Cipher.DECRYPT_MODE))
                    FingerprintManagerCompat.CryptoObject(cipher)
                else null
            }
        }
        return null
    }

    fun isSensorAvailable() : Boolean = fingerprintManagerCompat.isHardwareDetected && fingerprintManagerCompat.hasEnrolledFingerprints()

    fun start() {
        fingerprintManagerCompat.authenticate(getCryptoObject(), 0, cancellationSignal, this, null)
    }

    override fun onAuthenticationSucceeded(result: FingerprintManagerCompat.AuthenticationResult?) {
        super.onAuthenticationSucceeded(result)
        result?.let {
            val credentials = decryptString(credentialsRepo.getEncryptedData(), result.cryptoObject.cipher)
            credentials?.let {
                val l = credentials.substring(credentials.lastIndexOf("#") + 1, credentials.length).toInt()
                val email = credentials.substring(0, l)
                val pass = credentials.substring(email.length, credentials.length - l.toString().length - 1)
                onCredentialsDecrypted?.invoke(email, pass)
            }
            onAuthError?.invoke()
        }
        onAuthError?.invoke()
    }

    override fun onAuthenticationError(errMsgId: Int, errString: CharSequence?) {
        if (errMsgId  == FINGERPRINT_ERROR_LOCKOUT) {
            onAuthError?.invoke()
        }
        super.onAuthenticationError(errMsgId, errString)
    }

    fun cancel() {
        cancellationSignal.cancel()
    }

}
