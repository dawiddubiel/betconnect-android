package com.betconnect.app.extensions

import android.app.Activity
import android.content.Context
import android.graphics.PorterDuff
import android.net.ConnectivityManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.betconnect.app.R
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.bluelinelabs.conductor.changehandler.HorizontalChangeHandler
import com.bluelinelabs.conductor.changehandler.VerticalChangeHandler
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import java.text.SimpleDateFormat
import java.util.*

fun isEmailValid(email: String) = android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()

enum class Direction {
    Vertical, Horizontal
}

const val DEF_FADE_DURATION_MILIS = 200L

fun Router.push(controller: Controller, fadeDuration: Long? = null) {
    pushController(
        RouterTransaction
            .with(controller)
            .apply {
                fadeDuration?.let {
                    this.pushChangeHandler(FadeChangeHandler(fadeDuration))
                        .popChangeHandler(FadeChangeHandler(fadeDuration))
                }
            }
    )
}

fun Router.push(controller: Controller, direction: Direction?) {
    pushController(
        RouterTransaction
            .with(controller)
            .apply {
                when (direction) {
                    Direction.Horizontal -> {
                        pushChangeHandler(HorizontalChangeHandler())
                        popChangeHandler(HorizontalChangeHandler())
                    }
                    Direction.Vertical -> {
                        pushChangeHandler(VerticalChangeHandler())
                        popChangeHandler(VerticalChangeHandler())
                    }
                    null -> {
                        pushChangeHandler(FadeChangeHandler(DEF_FADE_DURATION_MILIS))
                        popChangeHandler(FadeChangeHandler(DEF_FADE_DURATION_MILIS))
                    }
                }
            }
    )
}

fun Router.replace(controller: Controller, direction: Direction?) {
    replaceTopController(
        RouterTransaction
            .with(controller)
            .apply {
                when (direction) {
                    Direction.Horizontal -> {
                        pushChangeHandler(HorizontalChangeHandler())
                        popChangeHandler(HorizontalChangeHandler())
                    }
                    Direction.Vertical -> {
                        pushChangeHandler(VerticalChangeHandler())
                        popChangeHandler(VerticalChangeHandler())
                    }
                    null -> {
                        pushChangeHandler(FadeChangeHandler(DEF_FADE_DURATION_MILIS))
                        popChangeHandler(FadeChangeHandler(DEF_FADE_DURATION_MILIS))
                    }
                }
            }
    )
}

fun Router.replace(controller: Controller, fadeDuration: Long? = null) {
    replaceTopController(
        RouterTransaction
            .with(controller)
            .apply {
                fadeDuration?.let {
                    pushChangeHandler(FadeChangeHandler(fadeDuration))
                    popChangeHandler(FadeChangeHandler(fadeDuration))
                }
            }
    )
}

fun Context.isNetworkAvailable() =
    (this.applicationContext?.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager)?.activeNetworkInfo?.isConnected
        ?: false


fun View.hideKeyboard() =
    (context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
        windowToken,
        0
    )

fun Controller.runOnUiThread(function: (Activity) -> Unit) {
    activity?.let {
        it.runOnUiThread { function.invoke(it) }
    }
}

fun View.makeVisible() {
    if (visibility != View.VISIBLE) visibility = View.VISIBLE
}

fun View.makeInvisible() {
    if (visibility == View.VISIBLE) visibility = View.INVISIBLE
}

fun View.makeGone() {
    if (visibility == View.VISIBLE) visibility = View.GONE
}

fun View.toggleVisibility(show: Boolean) {
    if (show) makeVisible() else makeGone()
}

fun getDateFormatted(day: Int, month: Int, year: Int): String =
    SimpleDateFormat("dd-MM-yyyy", Locale.UK).format(Calendar.getInstance().apply {
        set(year, month, day)
    }.time)


fun BottomNavigationMenuView.removePaddingFromNavigationItems() {
    for (i in 0..this.childCount) {
        val v =
            (this.getChildAt(i) as? BottomNavigationItemView)?.findViewById<View>(R.id.largeLabel)
        val tv = (v as? TextView)
        tv?.setPadding(0, 0, 0, 0)
    }
}

/*fun BottomNavigationMenuView.setFont(font: String = "fonts/Proxima-Nova-Regular.ttf") {
    for (i in 0..childCount) {
        val v = (this.getChildAt(i) as? BottomNavigationItemView)?.findViewById<View>(R.id.largeLabel)
        (v as? TextView)?.let {
            applyFontToTextView(it.context, it, font)
        }
    }
}*/

fun String.areOnlyLettersAndNumbers() = matches("[a-zA-Z0-9]*".toRegex())

fun EditText.changeBackground(resourceId: Int) {
    background = (ContextCompat.getDrawable(context, resourceId))
}

fun TextView.setBackgroundColorInt(color: Int) {
    background.mutate().setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
}