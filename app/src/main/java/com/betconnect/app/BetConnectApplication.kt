package com.betconnect.app

import androidx.multidex.MultiDexApplication
import com.betconnect.app.di.appModule
import com.betconnect.networking.di.networkingModule
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class BetConnectApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        if (!BuildConfig.DEBUG) {
            Fabric.with(this, Crashlytics())
        }
        else {
            Timber.plant(Timber.DebugTree())
        }
        startKoin {
            androidContext(this@BetConnectApplication)
            modules(listOf(appModule, networkingModule))
        }
        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/Proxima-Nova-Regular.ttf")
                            .setFontAttrId(R.attr.fontPath)
                            .build()
                    )
                )
                .build()
        )

    }

}