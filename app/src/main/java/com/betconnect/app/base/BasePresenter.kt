package com.betconnect.app.base

import com.betconnect.app.IBaseController
import com.betconnect.networking.ErrorType
import com.betconnect.networking.Result
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

interface IBasePresenter {
    fun start()
    fun destroy()
}


abstract class BasePresenter : IBasePresenter, CoroutineScope by MainScope() {

    override fun start() { }

    override fun destroy() {
        cancel()
    }

    protected suspend inline fun <T> Result<T>.onSuccess(
        crossinline block: suspend (T) -> Unit): Result<T> = apply { if (this is Result.Success<T>) block(value) }

    protected suspend inline fun <T> Result<T>.onError(
        crossinline block: suspend (ErrorType) -> Unit): Result<T> = apply { if (this is Result.Error<T>) block(value) }

    protected inline fun <T : IBaseController> T.lockAndLoad(
        crossinline block: suspend T.() -> Unit
    ) {
        launch {
            showProgress(true)
            block()
            showProgress(false)
        }
    }
}