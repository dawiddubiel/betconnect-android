package com.betconnect.app.base

import android.os.Bundle
import android.view.View
import com.betconnect.app.IBaseController
import com.betconnect.app.di.MAIN_ACTIVITY_SCOPE
import com.betconnect.app.di.MAIN_NAVIGATION
import com.betconnect.app.extensions.hideKeyboard
import com.betconnect.app.navigation.Navigation
import com.bluelinelabs.conductor.Controller
import com.google.android.material.snackbar.Snackbar
import org.koin.core.KoinComponent
import org.koin.core.qualifier.named

abstract class BaseController(args: Bundle? = null): IBaseController, Controller(args), KoinComponent {

    protected open val navigation: Navigation by lazy {
        getKoin().getScope(MAIN_ACTIVITY_SCOPE).get<Navigation>(named(MAIN_NAVIGATION))
    }

    override fun onDestroyView(view: View) {
        view.hideKeyboard()
        super.onDestroyView(view)
    }

    override fun showError(errorMessage: String) {
        view?.let {
            Snackbar.make(it, errorMessage, Snackbar.LENGTH_SHORT).show()
        }
    }

    override fun showProgress(show: Boolean) {
        navigation.showProgress(show)
    }

}
