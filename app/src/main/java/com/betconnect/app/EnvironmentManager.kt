package com.betconnect.app

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import android.provider.Settings
import androidx.core.content.ContextCompat
import com.betconnect.app.extensions.isNetworkAvailable

class EnvironmentManager(private val context: Context) {

    fun isLocationPermissionGranted() =
        ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED

    fun isGpsEnabled() =
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.P) {
            Settings.Secure.getInt(context.contentResolver, Settings.Secure.LOCATION_MODE) != 0
        } else {
            (context.getSystemService(
                Context.LOCATION_SERVICE
            ) as LocationManager).isProviderEnabled(
                LocationManager.GPS_PROVIDER
            ) || context.isNetworkAvailable()
        }
}