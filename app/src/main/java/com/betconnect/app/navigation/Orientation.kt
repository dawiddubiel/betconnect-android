package com.betconnect.app.navigation

enum class Orientation {
    Portrait,
    Landscape
}