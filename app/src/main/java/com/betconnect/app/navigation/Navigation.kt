package com.betconnect.app.navigation

import com.betconnect.app.extensions.Direction
import com.bluelinelabs.conductor.Controller

interface Navigation {

    fun showProgress(show: Boolean)
    fun show(controller: Controller, shouldReplace: Boolean = false, direction: Direction? = null)
    fun onBack()
    fun lockScreen(lockScreen: LockScreen)
    fun unlockScreen()
    fun showBiometricPrompt(show: Boolean)
    fun onCredentialsDecrypted(decryptedCredentials: String?)

}