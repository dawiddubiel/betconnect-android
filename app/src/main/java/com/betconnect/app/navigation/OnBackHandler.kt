package com.betconnect.app.navigation

interface OnBackHandler {
    fun onBack()
}