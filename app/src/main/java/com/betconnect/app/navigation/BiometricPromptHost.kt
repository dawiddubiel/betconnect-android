package com.betconnect.app.navigation

interface BiometricPromptHost {
    fun showBiometricPrompt(show: Boolean)
}