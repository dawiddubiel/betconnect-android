package com.betconnect.app.navigation

import android.view.ViewGroup
import com.betconnect.app.extensions.*
import com.betconnect.app.vp.login.LoginController
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.ControllerChangeHandler
import com.bluelinelabs.conductor.Router

class NavigationImpl(val navigationParams: NavigationParams) : Navigation {

    init {
        setupDialogRouter()
        setupLockScreenRouter()
    }

    override fun showProgress(show: Boolean) =
        navigationParams.mainViewHost.showProgress(show)

    override fun show(controller: Controller,
                      shouldReplace: Boolean,
                      direction: Direction?) {
        when (controller) {
            is Dialog -> navigationParams.dialogRouter
            else      -> navigationParams.mainRouter
        }.apply {
            if (currentController() != controller) {
                if (shouldReplace) {
                    replace(controller, direction)
                }
                else {
                    push(controller, direction)
                }
            }
        }
    }

    override fun onBack() {
        if (!navigationParams.dialogRouter.handleBack()) handleOnBack()
    }

    override fun showBiometricPrompt(show: Boolean) {
        navigationParams.biometricPromptHost.showBiometricPrompt(show)
    }

    override fun onCredentialsDecrypted(decryptedCredentials: String?) {
        (navigationParams.mainRouter.currentController() as? LoginController)?.run {
            this.onCredentialsDecrypted(decryptedCredentials)
        }
    }

    override fun lockScreen(lockScreen: LockScreen) {
        navigationParams.biometricPromptHost.showBiometricPrompt(false)
        navigationParams.lockRouter.replace(lockScreen as Controller)
    }

    override fun unlockScreen() {
        navigationParams.lockRouter.handleBack()
       // (navigationParams.mainRouter.currentController() as? LoginController)?.onMainScreenUnlocked()
    }

    private fun handleOnBack() {
        (navigationParams.mainRouter.backstack.lastOrNull()?.controller() as? OnBackHandler)?.onBack()
            ?: navigationParams.mainRouter.handleBack()
    }

    private fun setupLockScreenRouter() {
        navigationParams.lockRouter.setPopsLastView(true)
        navigationParams.lockRouter.addChangeListener(object : ControllerChangeHandler.ControllerChangeListener {
            override fun onChangeStarted(to: Controller?,
                                         from: Controller?,
                                         isPush: Boolean,
                                         container: ViewGroup,
                                         handler: ControllerChangeHandler
            ) {
                to?.let {
                    navigationParams.lockScreenView.makeVisible()
                } ?: navigationParams.lockScreenView.makeGone()
            }

            override fun onChangeCompleted(to: Controller?,
                                           from: Controller?,
                                           isPush: Boolean,
                                           container: ViewGroup,
                                           handler: ControllerChangeHandler
            ) {
            }
        })
    }

    private fun setupDialogRouter() {
        navigationParams.dialogRouter.setPopsLastView(true)
        navigationParams.dialogRouter.addChangeListener(object : ControllerChangeHandler.ControllerChangeListener {
            override fun onChangeStarted(
                to: Controller?,
                from: Controller?,
                isPush: Boolean,
                container: ViewGroup,
                handler: ControllerChangeHandler
            ) {
                to?.let {
                    navigationParams.dialogView.makeVisible()
                } ?: navigationParams.dialogView.apply {
                    makeGone()
                    navigationParams.dialogRouter.takeIf { it.backstackSize > 0 }?.popCurrentController()
                }
            }

            override fun onChangeCompleted(
                to: Controller?,
                from: Controller?,
                isPush: Boolean,
                container: ViewGroup,
                handler: ControllerChangeHandler
            ) {
            }
        })
    }

    private fun Router.currentController(): Controller? =
        backstack.lastOrNull()?.controller()

}