package com.betconnect.app.navigation

interface MainViewHost {
    fun showProgress(show: Boolean)
}