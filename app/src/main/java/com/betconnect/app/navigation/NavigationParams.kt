package com.betconnect.app.navigation

import android.view.ViewGroup
import com.bluelinelabs.conductor.Router

data class NavigationParams(val mainRouter: Router,
                            val dialogRouter: Router,
                            val lockRouter: Router,
                            val dialogView: ViewGroup,
                            val lockScreenView: ViewGroup,
                            val mainViewHost: MainViewHost,
                            val biometricPromptHost: BiometricPromptHost)