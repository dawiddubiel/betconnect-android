package com.betconnect.app.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.betconnect.app.R
import com.betconnect.app.base.BaseController
import com.betconnect.app.navigation.Dialog
import com.betconnect.networking.apiModel.registrationData.BirthDate
import kotlinx.android.synthetic.main.date_picker_controller.view.*
import kotlinx.android.synthetic.main.sign_up_account_details.view.*

class DatePickerController(args: Bundle? = null) : BaseController(args), Dialog {

    private var onDateSelected: ((Int, Int, Int) -> Unit)? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.date_picker_controller, container, false).apply {
            args.getParcelable<BirthDate?>(INITIAL_DATE)?.let {
                datePicker.init(it.yearOfBirth, it.monthOfBirth, it.dayOfBirth, null)
            }
            okBtn.setOnClickListener {
                onDateSelected?.invoke(datePicker.dayOfMonth, datePicker.month, datePicker.year)
                navigation.onBack()
            }
        }

    private fun setOnDateSelectedAction(func: (Int, Int, Int) -> Unit) {
        onDateSelected = func
    }

    companion object {
        private const val INITIAL_DATE = "initial_date"
        fun create(initialDate: BirthDate? = null, onDateSelected: (Int, Int, Int) -> Unit) =
            DatePickerController(Bundle().apply {
                putParcelable(
                    INITIAL_DATE,
                    initialDate
                )
            }).apply { setOnDateSelectedAction(onDateSelected) }
    }

}
