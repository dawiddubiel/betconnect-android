package com.betconnect.app.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.betconnect.app.R
import com.betconnect.app.base.BaseController
import com.betconnect.app.navigation.Dialog
import kotlinx.android.synthetic.main.info_dialog_layout.view.*

const val MESSAGE = "message"

class InfoDialogController(args: Bundle? = null) : BaseController(args), Dialog {

    private var onConfirm: (() -> Unit)? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.info_dialog_layout, container, false).apply {
            message.text = args.getString(MESSAGE)
            okBtn.setOnClickListener {
                onConfirm?.invoke()
                navigation.onBack()
            }
        }

    fun setOnConfirmAction(func: () -> Unit) {
        onConfirm = func
    }

    companion object {
        fun create(message: String, onConfirm: (() -> Unit)? = null) =
            InfoDialogController(Bundle().apply {
                putString(MESSAGE, message)
            }).apply {
                onConfirm?.let {
                    setOnConfirmAction(it)
                }
            }
    }

}