package com.betconnect.app.di

import android.content.Context
import android.location.Geocoder
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.biometric.BiometricPrompt
import androidx.fragment.app.FragmentActivity
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.betconnect.app.*
import com.betconnect.app.biometricAuth.BiometricAuthenticationCallback
import com.betconnect.app.biometricAuth.CredentialsRepoImpl
import com.betconnect.app.biometricAuth.isAtLeastApi23
import com.betconnect.app.fingerprint.ICredentialsRepo
import com.betconnect.app.geolocation.GeolocationManager
import com.betconnect.app.geolocation.IGeolocationManager
import com.betconnect.app.navigation.Navigation
import com.betconnect.app.navigation.NavigationImpl
import com.betconnect.app.navigation.NavigationParams
import com.betconnect.app.vp.IMainActivity
import com.betconnect.app.vp.MainActivityPresenter
import com.betconnect.app.vp.fingerprint.FingerprintQuestionPresenter
import com.betconnect.app.vp.fingerprint.FingerprintQuestionView
import com.betconnect.app.vp.fingerprint.IFingerprintQuestionPresenter
import com.betconnect.app.vp.forgotPassword.ForgotPasswordPresenter
import com.betconnect.app.vp.forgotPassword.ForgotPasswordView
import com.betconnect.app.vp.forgotPassword.IForgotPasswordPresenter
import com.betconnect.app.vp.login.ILoginPresenter
import com.betconnect.app.vp.login.LoginPresenter
import com.betconnect.app.vp.login.LoginView
import com.betconnect.app.vp.login.loginSettings.ILoginSettingsPresenter
import com.betconnect.app.vp.login.loginSettings.LoginSettingsPresenter
import com.betconnect.app.vp.login.loginSettings.LoginSettingsView
import com.betconnect.app.vp.signup.addressLookup.AddressLookupPresenter
import com.betconnect.app.vp.signup.addressLookup.AddressLookupView
import com.betconnect.app.vp.signup.addressLookup.IAddressLookupPresenter
import com.betconnect.app.vp.signup.step1.ISignUpStep1Presenter
import com.betconnect.app.vp.signup.step1.SignUpStep1Presenter
import com.betconnect.app.vp.signup.step1.SignUpStep1View
import com.betconnect.app.vp.signup.step2.ISignUpStep2Presenter
import com.betconnect.app.vp.signup.step2.SignUpStep2Presenter
import com.betconnect.app.vp.signup.step2.SignUpStep2View
import com.betconnect.app.vp.signup.step3.ISignUpStep3Presenter
import com.betconnect.app.vp.signup.step3.SignUpStep3Presenter
import com.betconnect.app.vp.signup.step3.SignUpStep3View
import com.betconnect.networking.apiModel.registrationData.RegistrationData
import com.google.android.gms.location.FusedLocationProviderClient
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import java.util.concurrent.Executors

const val MAX_LIABILITY_VALUES = "MAX_LIABILITY_VALUES"
const val DEPOSIT_LIMIT_DURATION_VALUES = "DEPOSIT_LIMIT_DURATION_VALUES"
const val PERSON_TITLES = "PERSON_TITLES"
const val EXTERNAL_URLS = "EXTERNAL_URLS"
const val MAIN_ACTIVITY_SCOPE = "MAIN_ACTIVITY_SCOPE"
const val MAIN_NAVIGATION = "MAIN_NAVIGATION"

@RequiresApi(Build.VERSION_CODES.M)
val appModule = module {

    scope(named(MAIN_ACTIVITY_SCOPE)) {
        scoped<Navigation>(named(MAIN_NAVIGATION)) { (navigationParams: NavigationParams) ->
            NavigationImpl(navigationParams)
        }
    }

    factory<IForgotPasswordPresenter> { (view: ForgotPasswordView) ->
        ForgotPasswordPresenter(
            view,
            get()
        )
    }

    factory<ILoginPresenter> { (view: LoginView) ->
        LoginPresenter(
            view = view,
            cookieManager = get(),
            fingerprintPreferences = get(),
            credentialsRepo = get(),
            remoteDataSource = get()
        )
    }

    factory<IFingerprintQuestionPresenter> { (view: FingerprintQuestionView, encryptedCredentials: String) ->
        FingerprintQuestionPresenter(
            view,
            encryptedCredentials,
            get(),
            get()
        )
    }

    factory<ISignUpStep1Presenter> { (view: SignUpStep1View, registrationData: RegistrationData) ->
        SignUpStep1Presenter(
            view,
            registrationData
        )
    }
    factory<ISignUpStep2Presenter> { (view: SignUpStep2View, registrationData: RegistrationData) ->
        SignUpStep2Presenter(
            view,
            registrationData
        )
    }
    factory<ISignUpStep3Presenter> { (view: SignUpStep3View, registrationData: RegistrationData) ->
        SignUpStep3Presenter(
            view,
            registrationData,
            get(),
            get()
        )
    }

    factory<IAddressLookupPresenter> { (view: AddressLookupView, registrationData: RegistrationData) ->
        AddressLookupPresenter(
            view,
            registrationData,
            get()
        )
    }

    single<IGeolocationManager> {
        GeolocationManager(
            fusedLocationProviderClient = get(),
            geocoder = get(),
            allowedLocations = androidApplication().resources.getStringArray(R.array.non_restricted_locations).toList()
        )
    }
    single { FusedLocationProviderClient(androidApplication()) }

    single { Geocoder(androidApplication()) }

    factory { (view: IMainActivity) ->
        MainActivityPresenter(
            view = view,
            appPreferences = get(),
            fingerprintPreferences = get(),
            credentialsRepo = get(),
            geolocationManager = get(),
            environmentManager = get()
        )
    }

    single<IAppPreferences> {
        AppPreferences(
            androidContext().getSharedPreferences(
                BETCONNECT_PREFERENCES,
                Context.MODE_PRIVATE
            )
        )
    }
    single<FingerprintPreferences> {
        AppPreferences(
            androidContext().getSharedPreferences(
                BETCONNECT_PREFERENCES,
                Context.MODE_PRIVATE
            )
        )
    }
    /*single<ICredentialsRepo> {
        AppPreferences(
            androidContext().getSharedPreferences(
                BETCONNECT_PREFERENCES,
                Context.MODE_PRIVATE
            )
        )
    }*/

    single {
        if (isAtLeastApi23()) {
            CredentialsRepoImpl(
                EncryptedSharedPreferences.create(
                    "internal2",
                    MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
                    androidApplication(),
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
                )
            )

        } else object : ICredentialsRepo {
            override fun clearCredentialsData() {}
            override fun setCredentials(credentials: String) {}
            override fun getCredentials(): String? = null
        }
    }

    single { EnvironmentManager(androidApplication()) }

    factory<ILoginSettingsPresenter> { (view: LoginSettingsView) ->
        LoginSettingsPresenter(
            view = view,
            fingerprintPreferences = get()
        )
    }

    single(named(MAX_LIABILITY_VALUES)) {
        androidContext().resources.getIntArray(R.array.maximum_requests_array).toList()
    }

    single(named(DEPOSIT_LIMIT_DURATION_VALUES)) {
        androidContext().resources.getStringArray(R.array.deposit_limit_duration).toList()
    }

    single(named(PERSON_TITLES)) {
        androidContext().resources.getStringArray(R.array.titles_array).toList()
    }

    single(named(EXTERNAL_URLS)) {
        androidContext().resources.getStringArray(R.array.app_external_urls).toList()
    }

    factory { (activity: FragmentActivity, onTaskStart: () -> Unit, onSuccess: (String?) -> Unit, onError: () -> Unit) ->
        BiometricPrompt(
            activity, Executors.newSingleThreadExecutor(),
            BiometricAuthenticationCallback(
                credentialsRepo = get(),
                fingerprintPreferences = get(),
                onTaskStart = onTaskStart,
                onSuccess = onSuccess,
                onError = onError
            )
        )
    }
    factory {
        BiometricPrompt.PromptInfo.Builder()
            .setTitle(androidContext().getString(R.string.biometric_prompt_title))
            .setDescription(androidContext().getString(R.string.biometric_prompt_description))
            .setNegativeButtonText(
                androidContext().getString(R.string.biometric_prompt_negative_button)
            )
            .build()
    }

}
