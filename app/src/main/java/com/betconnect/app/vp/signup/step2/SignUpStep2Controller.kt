package com.betconnect.app.vp.signup.step2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.betconnect.app.R
import com.betconnect.app.base.BaseController
import com.betconnect.app.custom.MaxLiabilityPresentation
import com.betconnect.app.custom.itemSelectionsFlow
import com.betconnect.app.custom.textChangedFlow
import com.betconnect.app.di.MAX_LIABILITY_VALUES
import com.betconnect.app.dialog.DatePickerController
import com.betconnect.app.extensions.*
import com.betconnect.app.navigation.OnBackHandler
import com.betconnect.app.vp.signup.sport_preferences.SportPreferencesController
import com.betconnect.app.vp.signup.step1.SignUpStep1Controller
import com.betconnect.app.vp.signup.step3.SignUpStep3Controller
import com.betconnect.networking.apiModel.registrationData.BirthDate
import com.betconnect.networking.apiModel.registrationData.REGISTRATION_DATA
import com.betconnect.networking.apiModel.registrationData.RegistrationData
import com.betconnect.networking.apiModel.registrationData.UserRole
import kotlinx.android.synthetic.main.sign_up_account_details.view.*
import kotlinx.coroutines.flow.map
import org.koin.core.get
import org.koin.core.inject
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named

interface SignUpStep2View {
    fun onInputsValid(valid: Boolean)
    fun invalidateWarnings()
    fun showPasswordToShortWarning(show: Boolean)
    fun showConfirmPasswordNotMatchWarning(show: Boolean)
    fun showUserToYoungWarning(show: Boolean)
    fun fillOutViews(registrationData: RegistrationData)
    fun proceedToNextStep(registrationData: RegistrationData)
    fun showUsernameInvalidWarning(show: Boolean)
}

class SignUpAccountDetails(args: Bundle) : BaseController(args), SignUpStep2View, OnBackHandler {

    private val presenter: ISignUpStep2Presenter by inject {
        parametersOf(this, args.getParcelable(REGISTRATION_DATA) as RegistrationData)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.sign_up_account_details, container, false).apply {
            maxLiabilitySpinner.adapter = ArrayAdapter(
                context,
                R.layout.spinner_item,
                get<List<Int>>(named(MAX_LIABILITY_VALUES)).map { MaxLiabilityPresentation(it) }
            ).apply {
                setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            }

            birthDateTv.setOnClickListener {
                hideKeyboard()
                navigation.show(DatePickerController.create(presenter.getRegistrationData().birthdate) { day, month, year ->
                    birthDateTv.text = getDateFormatted(day, month, year)
                })
            }

            nextBtn.setOnClickListener { presenter.onNext() }
            previousBtn.setOnClickListener { navigation.onBack() }
            presenter.consumeInputs(
                handleEt.textChangedFlow(true),
                passwordEt.textChangedFlow(true),
                confirmPasswordEt.textChangedFlow(true),
                birthDateTv.textChangedFlow(true)
                    .map { dateFormatted ->
                        dateFormatted.takeIf { it.isNotBlank() }?.let { BirthDate(it) }
                    },
                maxLiabilitySpinner.itemSelectionsFlow()
            )
        }

    override fun onAttach(view: View) {
        super.onAttach(view)
        presenter.start()
    }

    override fun onDestroy() {
        presenter.destroy()
        super.onDestroy()
    }

    override fun fillOutViews(registrationData: RegistrationData) {
        view?.let { view ->
            registrationData.run {
                handle?.let { view.handleEt.setText(it) }
                password?.let { view.passwordEt.setText(it) }
                passwordConfirm?.let { view.confirmPasswordEt.setText(it) }
                birthdate?.let { birthDate ->
                    view.birthDateTv.text = getDateFormatted(
                        birthDate.dayOfBirth,
                        birthDate.monthOfBirth,
                        birthDate.yearOfBirth
                    )
                }
                maxLiabilityInCents?.let { maxLiabilityInCents ->
                    val selection = get<List<Int>>(
                        named(
                            MAX_LIABILITY_VALUES
                        )
                    ).map { MaxLiabilityPresentation(it) }.indexOf(
                        MaxLiabilityPresentation(maxLiabilityInCents)
                    )
                    view.maxLiabilitySpinner.setSelection(selection)
                }
            }
        }
    }

    override fun onInputsValid(valid: Boolean) {
        view?.nextBtn?.isEnabled = valid
    }

    override fun invalidateWarnings() {
        showPasswordToShortWarning(false)
        showConfirmPasswordNotMatchWarning(false)
        showUserToYoungWarning(false)
        showUsernameInvalidWarning(false)
    }

    override fun showPasswordToShortWarning(show: Boolean) {
        view?.passwordEt?.changeBackground(if (show) R.drawable.background_red_rounded else R.drawable.edittext_background)
    }

    override fun showConfirmPasswordNotMatchWarning(show: Boolean) {
        view?.confirmPasswordEt?.changeBackground(if (show) R.drawable.background_red_rounded else R.drawable.edittext_background)
    }

    override fun showUsernameInvalidWarning(show: Boolean) {
        view?.handleEt?.changeBackground(if (show) R.drawable.background_red_rounded else R.drawable.edittext_background)
    }

    override fun showUserToYoungWarning(show: Boolean) {
        if (show) view?.birthDateWarning?.makeVisible() else view?.birthDateWarning?.makeGone()
    }

    override fun proceedToNextStep(registrationData: RegistrationData) {
        navigation.show(
            if (registrationData.role == UserRole.Punter) {
                SportPreferencesController.create(registrationData)
            } else {
                SignUpStep3Controller.create(registrationData)
            }
        )
    }

    companion object {
        fun create(registrationData: RegistrationData) = SignUpAccountDetails(Bundle().apply {
            putParcelable(REGISTRATION_DATA, registrationData)
        })
    }

    override fun onBack() {
        navigation.show(
            SignUpStep1Controller.create(presenter.getRegistrationData()),
            shouldReplace = true
        )
    }

}
