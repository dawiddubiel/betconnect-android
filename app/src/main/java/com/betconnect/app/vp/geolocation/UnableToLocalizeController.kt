package com.betconnect.app.vp.geolocation

import android.content.Intent
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.betconnect.app.R
import com.betconnect.app.base.BaseController
import com.betconnect.app.navigation.LockScreen
import com.betconnect.app.navigation.OnBackHandler
import kotlinx.android.synthetic.main.unable_to_find_localization_layout.view.*

class UnableToLocalizeController : BaseController(), LockScreen, OnBackHandler {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.unable_to_find_localization_layout, container, false).apply {
            updateSettingsBtn.setOnClickListener {
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }
        }

    override fun onBack() {
        activity?.moveTaskToBack(false)
    }

}
