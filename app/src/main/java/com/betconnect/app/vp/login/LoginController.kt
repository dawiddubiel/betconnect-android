package com.betconnect.app.vp.login

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.betconnect.app.IBaseController
import com.betconnect.app.R
import com.betconnect.app.base.BaseController
import com.betconnect.app.biometricAuth.Credentials
import com.betconnect.app.custom.setAllOnClickListener
import com.betconnect.app.extensions.hideKeyboard
import com.betconnect.app.extensions.makeGone
import com.betconnect.app.extensions.makeVisible
import com.betconnect.app.extensions.toggleVisibility
import com.betconnect.app.navigation.OnBackHandler
import com.betconnect.app.vp.MainActivity
import com.betconnect.app.vp.forgotPassword.ForgotPasswordController
import com.betconnect.app.vp.login.loginSettings.LoginSettingsController
import com.betconnect.app.vp.signup.step1.SignUpStep1Controller
import com.betconnect.app.vp.webView.WebViewController
import com.betconnect.networking.apiModel.registrationData.UserRole
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import kotlinx.android.synthetic.main.login_screen.view.*
import org.koin.core.inject
import org.koin.core.parameter.parametersOf
import timber.log.Timber

interface LoginView : IBaseController {
    fun onLoginSuccess(userRole: UserRole, param: String?)
    fun showLoginFailedError()
    fun showEmptyLogin()
    fun showEmptyPassword()
    fun invalidateErrors()
    fun startOnboarding()
    fun onLocationUnavailable()
    fun enableLoginSettings(enable: Boolean)
    fun showTimeoutError()
    fun showBiometricPrompt()
}

class LoginController : BaseController(), LoginView, OnBackHandler {

    private val presenter: ILoginPresenter by inject { parametersOf(this) }


    @SuppressLint("HardwareIds")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.login_screen, container, false).apply {

            loginSettingsGroup.setAllOnClickListener { navigation.show(LoginSettingsController()) }

            forgottenPasswordTV.setOnClickListener { navigation.show(ForgotPasswordController()) }

            loginBtn.setOnClickListener {
                hideKeyboard()
                presenter.attemptLogin(
                    login = emailEt.text.toString(),
                    password = passwordEt.text.toString(),
                    withFingerprint = false
                )
            }
            signUpTV.setOnClickListener { navigation.show(SignUpStep1Controller()) }
        }

    override fun onAttach(view: View) {
        super.onAttach(view)
        presenter.start()
    }

    override fun onLoginSuccess(userRole: UserRole, param: String?) {
        param?.let { (activity as? MainActivity)?.showFingerprintQuestion(it) }
        navigation.show(WebViewController.create(userRole = userRole), shouldReplace = true)
    }

    override fun showLoginFailedError() {
        view?.signInErrorTv?.makeVisible()
    }

    override fun showEmptyLogin() {
        view?.loginErrorTv?.makeVisible()
    }

    override fun showEmptyPassword() {
        view?.passwordError?.makeVisible()
    }

    override fun invalidateErrors() {
        view?.apply {
            signInErrorTv?.makeGone()
            passwordError?.makeGone()
            loginErrorTv?.makeGone()
            timeoutErrorTv?.makeGone()
        }
    }

    override fun onDetach(view: View) {
        Timber.d("onDetach")
        navigation.showBiometricPrompt(false)
        super.onDetach(view)
    }

    override fun onDestroy() {
        Timber.d("onDestroy")
        presenter.destroy()
        super.onDestroy()
    }

    override fun onDestroyView(view: View) {
        Timber.d("onDestroyView")
        super.onDestroyView(view)
    }

    override fun startOnboarding() {
        (activity as? MainActivity)?.startOnboarding()
    }

    override fun onLocationUnavailable() {
        (activity as? MainActivity)?.showUnallowedLocationWarning()
    }

    override fun enableLoginSettings(enable: Boolean) {
        view?.loginSettingsGroup?.toggleVisibility(enable)
    }

    override fun showTimeoutError() {
        view?.timeoutErrorTv?.makeVisible()
    }

    override fun showBiometricPrompt() {
        navigation.showBiometricPrompt(true)
    }

    override fun onBack() {
        activity?.moveTaskToBack(false)
    }

    fun onCredentialsDecrypted(decryptedCredentials: String?) {
        jacksonObjectMapper().readValue(decryptedCredentials, Credentials::class.java)?.run {
            presenter.attemptLogin(
                login,
                password,
                true
            )
        }
    }

}
