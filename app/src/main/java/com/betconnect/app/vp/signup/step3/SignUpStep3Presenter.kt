package com.betconnect.app.vp.signup.step3

import android.webkit.CookieManager
import com.betconnect.app.base.BasePresenter
import com.betconnect.app.base.IBasePresenter
import com.betconnect.networking.LOCATION_FAILED
import com.betconnect.networking.RemoteDataSource
import com.betconnect.networking.apiModel.registrationData.EDepositLimitDuration
import com.betconnect.networking.apiModel.registrationData.RegistrationData
import com.betconnect.networking.di.BASE_URL
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import timber.log.Timber
import java.util.regex.Pattern

const val UK_PHONE_REGEX =
    "^(((\\+44\\s?\\d{4}|\\(?0\\d{4}\\)?)\\s?\\d{3}\\s?\\d{3})|((\\+44\\s?\\d{3}|\\(?0\\d{3}\\)?)\\s?\\d{3}\\s?\\d{4})|((\\+44\\s?\\d{2}|\\(?0\\d{2}\\)?)s?\\d{4}\\s?\\d{4}))(\\s?\\#(\\d{4}|\\d{3}))?$"

interface ISignUpStep3Presenter : IBasePresenter {
    fun createAccount()
    fun getRegistrationData(): RegistrationData
    fun consumeInputs(
        phoneNumberFlow: Flow<String>,
        limitAmountFlow: Flow<String>,
        limitDurationFlow: Flow<EDepositLimitDuration?>
    )
}

class SignUpStep3Presenter(
    private val view: SignUpStep3View,
    private val registrationData: RegistrationData,
    private val remoteDataSource: RemoteDataSource,
    private val cookieManager: CookieManager
) : ISignUpStep3Presenter, BasePresenter() {

    override fun start() {
        super.start()
        view.fillOutViews(registrationData)
    }

    override fun consumeInputs(
        phoneNumberFlow: Flow<String>,
        limitAmountFlow: Flow<String>,
        limitDurationFlow: Flow<EDepositLimitDuration?>
    ) {
        combine(
            phoneNumberFlow,
            limitAmountFlow,
            limitDurationFlow
        ) { phoneNumber, limitAmount, limitDuration ->
            validateInputs(phoneNumber, limitAmount, limitDuration)
        }
            .launchIn(this)
    }


    private fun validateInputs(
        phoneNumber: String,
        depositLimitAmount: String,
        limitDuration: EDepositLimitDuration?
    ) {
        registrationData.apply {
            this.contactNumber = phoneNumber
            depositLimitAmount.toIntOrNull()?.let {
                this.depositLimitAmount = it * 100
            }
            this.depositLimitDuration = limitDuration
        }
        view.run {
            showLimitAmountView(limitDuration != EDepositLimitDuration.UNLIMITED)
            onInputsValid(
                isValidNumber(phoneNumber) && registrationData.userAddress.postalCode != null && limitDuration != null
                        && ((limitDuration == EDepositLimitDuration.UNLIMITED ||
                        (limitDuration != EDepositLimitDuration.UNLIMITED && registrationData.depositLimitAmount != null)))
            )
        }
    }

    override fun createAccount() {
        view.lockAndLoad {
            remoteDataSource.signUp(registrationData)
                .onSuccess {
                    cookieManager.setCookie(BASE_URL, it)
                    view.showWebView(registrationData.role!!)
                }
                .onError {
                    when (it.message) {
                        LOCATION_FAILED -> view.showLocationRestrictedException()
                        else -> view.showError(it.message)
                    }
                }
        }
    }

    override fun getRegistrationData() = registrationData

    private fun isValidNumber(phoneNumber: String) =
        Pattern.compile(UK_PHONE_REGEX).matcher(phoneNumber).matches()

}
