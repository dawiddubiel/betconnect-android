package com.betconnect.app.vp.signup.sport_preferences

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.betconnect.app.R
import com.betconnect.app.extensions.makeGone
import com.betconnect.app.extensions.makeVisible
import com.betconnect.networking.apiModel.registrationData.SportPreferencesListItem
import kotlinx.android.synthetic.main.sport_preferences_list_item.view.*

class SportPreferencesAdapter(
    val sportPreferences: List<SportPreferencesListItem>,
    private val onSelectionChanged: (Boolean) -> Unit
) :
    RecyclerView.Adapter<SportsListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = SportsListViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.sport_preferences_list_item,
            parent,
            false
        )
    )

    override fun getItemCount() = sportPreferences.size

    override fun onBindViewHolder(holder: SportsListViewHolder, position: Int) =
        holder.bind(sportPreferences[position], onSelectionChanged, sportPreferences)

}

class SportsListViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    fun bind(
        item: SportPreferencesListItem,
        onSelectionChanged: (Boolean) -> Unit,
        sportPreferences: List<SportPreferencesListItem>
    ) {
        view.apply {
            sportNameTv.text = item.option.displayName
            if (item.selected) {
                itemView.background =
                    ContextCompat.getDrawable(itemView.context, R.drawable.button_blue_rounded)
                sportNameTv.setTextColor(ContextCompat.getColor(itemView.context, R.color.white))
                itemView.isSelected = true
                checkIV.makeVisible()
            } else {
                itemView.background =
                    ContextCompat.getDrawable(itemView.context, R.drawable.button_rounded_stroke)
                sportNameTv.setTextColor(ContextCompat.getColor(itemView.context, R.color.black))
                itemView.isSelected = false
                checkIV.makeGone()
            }
            itemView.setOnClickListener {
                it.isSelected = !it.isSelected
                if (it.isSelected) {
                    item.selected = true
                    it.background =
                        ContextCompat.getDrawable(it.context, R.drawable.button_blue_rounded)
                    sportNameTv.setTextColor(ContextCompat.getColor(it.context, R.color.white))
                    checkIV.makeVisible()
                } else {
                    item.selected = false
                    it.background =
                        ContextCompat.getDrawable(it.context, R.drawable.button_rounded_stroke)
                    sportNameTv.setTextColor(ContextCompat.getColor(it.context, R.color.black))
                    checkIV.makeGone()
                }
                onSelectionChanged(sportPreferences.any { it.selected })
            }

        }
    }

}
