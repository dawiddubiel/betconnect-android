package com.betconnect.app.vp.forgotPassword

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.betconnect.app.IBaseController
import com.betconnect.app.R
import com.betconnect.app.base.BaseController
import com.betconnect.app.dialog.InfoDialogController
import com.betconnect.app.extensions.hideKeyboard
import com.betconnect.app.extensions.makeGone
import com.betconnect.app.extensions.makeVisible
import com.betconnect.app.navigation.OnBackHandler
import com.betconnect.app.vp.login.LoginController
import kotlinx.android.synthetic.main.forgot_password_layout.view.*
import org.koin.core.inject
import org.koin.core.parameter.parametersOf

interface ForgotPasswordView : IBaseController {
    fun backToLoginView()
    fun onPasswordResetSuccess()
    fun onPasswordResetError(message: String)
}

class ForgotPasswordController : BaseController(), ForgotPasswordView, OnBackHandler {

    private val presenter: IForgotPasswordPresenter by inject { parametersOf(this) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.forgot_password_layout, container, false).apply {

            rememberedPasswordTv.setOnClickListener { navigation.onBack() }
            resetPasswordBtn.setOnClickListener {
                if (emailEt.text.length < 3) {
                    passwordError.makeVisible()
                    return@setOnClickListener
                }
                passwordError.makeGone()
                it.hideKeyboard()
                presenter.resetPassword(emailEt.text.toString())
            }
        }

    override fun backToLoginView() {
        navigation.show(LoginController())
    }

    override fun onPasswordResetSuccess() {
        view?.let {
            navigation.show(InfoDialogController.create(it.context.getString(R.string.reset_password_success)) {
                navigation.show(LoginController())
            })
        }
    }

    override fun onPasswordResetError(message: String) {
        showError(message)
    }

    override fun onBack() {
        navigation.show(LoginController(), shouldReplace = true)
    }

}
