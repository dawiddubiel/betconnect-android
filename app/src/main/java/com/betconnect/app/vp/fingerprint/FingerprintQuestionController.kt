package com.betconnect.app.vp.fingerprint

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.betconnect.app.IBaseController
import com.betconnect.app.INTENT_CUSTOM_PARAM
import com.betconnect.app.R
import com.betconnect.app.base.BaseController
import com.betconnect.app.navigation.Dialog
import kotlinx.android.synthetic.main.fingerprint_question_layout.view.*
import org.koin.core.inject
import org.koin.core.parameter.parametersOf

interface FingerprintQuestionView: IBaseController {
    fun onQuestionAnswered()
}

class FingerprintQuestionController(args: Bundle) : BaseController(args), FingerprintQuestionView,
    Dialog {

    private val presenter: IFingerprintQuestionPresenter by inject {
        parametersOf(
            this,
            args.getString(INTENT_CUSTOM_PARAM, null)
        )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.fingerprint_question_layout, container, false).apply {
            turnFingerprintOnBtn.setOnClickListener { presenter.turnOnFingerprint() }
            notNowBtn.setOnClickListener { presenter.notNow() }
        }

    override fun onQuestionAnswered() {
        navigation.onBack()
    }

    companion object {
        fun create(param: String) = FingerprintQuestionController(Bundle().apply {
            putString(INTENT_CUSTOM_PARAM, param)
        })
    }

}
