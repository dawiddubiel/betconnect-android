package com.betconnect.app.vp.signup.addressLookup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.betconnect.app.IBaseController
import com.betconnect.app.R
import com.betconnect.app.base.BaseController
import com.betconnect.app.custom.textChangedFlow
import com.betconnect.app.vp.signup.step3.SignUpStep3Controller
import com.betconnect.networking.address.AddressResult
import com.betconnect.networking.apiModel.registrationData.REGISTRATION_DATA
import com.betconnect.networking.apiModel.registrationData.RegistrationData
import kotlinx.android.synthetic.main.address_lookup_layout.view.*
import org.koin.core.inject
import org.koin.core.parameter.parametersOf


interface AddressLookupView : IBaseController {
    fun updateResults(addresses: List<AddressResult>)
    fun onAddressSelected(regData: RegistrationData)
}

class AddressLookupController(args: Bundle) : BaseController(args), AddressLookupView {

    private val presenter: IAddressLookupPresenter by inject {
        parametersOf(this, args.get(REGISTRATION_DATA) as RegistrationData)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.address_lookup_layout, container, false).apply {
            setupRecyclerView(this)
            presenter.consumeInputs(addressInput.textChangedFlow())
        }

    override fun onDestroy() {
        presenter.destroy()
        super.onDestroy()
    }

    private fun setupRecyclerView(view: View) {
        val ltManager = LinearLayoutManager(view.context)
        view.recyclerView.apply {
            layoutManager = ltManager
            adapter = AddressLookupAdapter(emptyList()) {
                presenter.onResultSelected(it, view.addressInput.text.toString())
            }
            addItemDecoration(
                DividerItemDecoration(view.recyclerView.context, ltManager.orientation)
            )
        }
    }

    override fun updateResults(addresses: List<AddressResult>) {
        view?.apply { (recyclerView.adapter as AddressLookupAdapter).setData(addresses) }
    }

    override fun onAddressSelected(regData: RegistrationData) {
        navigation.show(SignUpStep3Controller.create(regData), shouldReplace = true)
    }

    companion object {
        fun create(registrationData: RegistrationData) = AddressLookupController(Bundle().apply {
            putParcelable(REGISTRATION_DATA, registrationData)
        })
    }

}
