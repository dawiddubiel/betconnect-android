package com.betconnect.app.vp

import com.betconnect.app.EnvironmentManager
import com.betconnect.app.FingerprintPreferences
import com.betconnect.app.IAppPreferences
import com.betconnect.app.base.BasePresenter
import com.betconnect.app.fingerprint.ICredentialsRepo
import com.betconnect.app.geolocation.IGeolocationManager
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch

interface IMainActivityPresenter {
    fun start()
    fun setOnboardingCompleted()
    fun stop()
    fun startMonitoringLocation()
    fun onUserLocationPermissionDenied()
    fun shouldAskForLocationPermission(): Boolean
    fun shouldShowFingerprintQuestion(): Boolean
    fun isFingerprintSettingOn(): Boolean
    fun stopMonitoringLocation()
    fun processLocation()
}

class MainActivityPresenter(
    private val view: IMainActivity,
    private val appPreferences: IAppPreferences,
    private val fingerprintPreferences: FingerprintPreferences,
    private val credentialsRepo: ICredentialsRepo,
    private val geolocationManager: IGeolocationManager,
    private val environmentManager: EnvironmentManager
) : IMainActivityPresenter, BasePresenter() {

    override fun processLocation() {
        when {
            !environmentManager.isGpsEnabled() -> view.onGpsDisabled()
            environmentManager.isLocationPermissionGranted() -> startMonitoringLocation()
            !appPreferences.shouldAskForLocationPermission() -> view.onLocationDisabled()
            else -> view.showLocationPermissionRationale()
        }
    }

    override fun startMonitoringLocation() {
        geolocationManager.requestLocationUpdates()
        launch {
            geolocationManager.allowedLocationChannel
                .consumeEach { locationAllowed ->
                    if (!locationAllowed) view.showUnallowedLocationWarning()
                    else view.onAllowedLocalization()
                }
        }
    }

    override fun setOnboardingCompleted() {
        appPreferences.setOnboardingCompleted(true)
    }

    override fun onUserLocationPermissionDenied() {
        appPreferences.setShouldAskForLocationPermission(false)
        view.onLocationDisabled()
    }

    override fun shouldAskForLocationPermission() = appPreferences.shouldAskForLocationPermission()

    override fun shouldShowFingerprintQuestion() =
        !fingerprintPreferences.isFingerprintSettingSet() && credentialsRepo.getCredentials() == null

    override fun isFingerprintSettingOn() = fingerprintPreferences.isFingerprintLoginAllowedByUser()

    override fun stopMonitoringLocation() {
        geolocationManager.stopLocationUpdates()
    }

    override fun stop() {
        geolocationManager.stopLocationUpdates()
    }

}