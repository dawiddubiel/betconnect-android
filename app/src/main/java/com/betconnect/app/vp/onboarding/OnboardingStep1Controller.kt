package com.betconnect.app.vp.onboarding

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.betconnect.app.R
import com.betconnect.app.base.BaseController
import kotlinx.android.synthetic.main.onboarding_step_one.view.*

class OnboardingStep1Controller : BaseController() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View
            = inflater.inflate(R.layout.onboarding_step_one, container, false).apply {

        nextBtn.setOnClickListener {
            navigation.show(OnboardingStep2Controller(), shouldReplace = true)
        }

    }

}
