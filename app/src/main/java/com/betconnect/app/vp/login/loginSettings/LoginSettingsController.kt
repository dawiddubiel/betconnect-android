package com.betconnect.app.vp.login.loginSettings

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.betconnect.app.R
import com.betconnect.app.base.BaseController
import com.betconnect.app.custom.setAllOnClickListener
import kotlinx.android.synthetic.main.login_settings_layout.view.*
import org.koin.core.inject
import org.koin.core.parameter.parametersOf

interface LoginSettingsView {
    fun indicateFingerprintEnabled(fingerprintLoginAllowedByUser: Boolean)
}

class LoginSettingsController : BaseController(), LoginSettingsView {

    private val presenter by inject<ILoginSettingsPresenter> {
        parametersOf(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.login_settings_layout, container, false).apply {
            fingerprintLoginSwitch.setOnCheckedChangeListener { _, isChecked ->
                presenter.setFingerprintEnabled(isChecked)
            }
            onBackGroup.setAllOnClickListener { navigation.onBack() }
        }

    override fun onAttach(view: View) {
        super.onAttach(view)
        presenter.start()
    }

    override fun indicateFingerprintEnabled(fingerprintLoginAllowedByUser: Boolean) {
        view?.fingerprintLoginSwitch?.isChecked = fingerprintLoginAllowedByUser
    }

}
