package com.betconnect.app.vp.signup.addressLookup

import com.betconnect.app.base.BasePresenter
import com.betconnect.app.base.IBasePresenter
import com.betconnect.networking.RemoteDataSource
import com.betconnect.networking.address.AddressResult
import com.betconnect.networking.address.RESULT_TYPE_ADDRESS
import com.betconnect.networking.apiModel.registrationData.RegistrationData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

interface IAddressLookupPresenter : IBasePresenter {
    fun retrieveLocation(id: String)
    fun onResultSelected(result: AddressResult, input: String)
    fun consumeInputs(addressInput: Flow<String>)
}

class AddressLookupPresenter(
    private val view: AddressLookupView,
    private val regData: RegistrationData,
    private val remoteDataSource: RemoteDataSource
) : IAddressLookupPresenter, BasePresenter() {

    override fun consumeInputs(addressInput: Flow<String>) {
        addressInput.onEach { lookForAddress(it) }.debounce(600).launchIn(this)
    }

    private fun lookForAddress(input: String, container: String? = "") {
        if (input.isEmpty()) {
            view.updateResults(emptyList())
        } else
            view.lockAndLoad {
                remoteDataSource.getLocation(input, container)
                    .onSuccess { results -> view.updateResults(addresses = results.items) }
                    .onError { view.showError(it.message) }
            }
    }

    override fun retrieveLocation(id: String) {
        view.lockAndLoad {
            remoteDataSource.retrieveLocation(id)
                .onSuccess { addresses ->
                    addresses.items.firstOrNull()?.run {
                        regData.userAddress.apply {
                            street = Street
                            city = City
                            subCity = AdminAreaName
                            postalCode = PostalCode
                            building = BuildingNumber
                            premise = BuildingName
                        }
                        view.onAddressSelected(regData)
                    }

                }
                .onError { view.showError(it.message) }
        }
    }

    override fun onResultSelected(result: AddressResult, input: String) {
        if (result.type == RESULT_TYPE_ADDRESS) {
            retrieveLocation(result.id!!)
        } else {
            lookForAddress(input, result.id)
        }
    }

}
