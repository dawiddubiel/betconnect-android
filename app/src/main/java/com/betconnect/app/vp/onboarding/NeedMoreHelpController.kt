package com.betconnect.app.vp.onboarding

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.betconnect.app.R
import com.bluelinelabs.conductor.Controller

class NeedMoreHelpController : Controller() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        return inflater.inflate(R.layout.need_more_help, container, false)
    }

}
