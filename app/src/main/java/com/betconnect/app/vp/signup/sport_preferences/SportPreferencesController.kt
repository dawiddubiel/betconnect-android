package com.betconnect.app.vp.signup.sport_preferences

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.betconnect.app.R
import com.betconnect.app.base.BaseController
import com.betconnect.app.navigation.OnBackHandler
import com.betconnect.app.vp.signup.step2.SignUpAccountDetails
import com.betconnect.app.vp.signup.step3.SignUpStep3Controller
import com.betconnect.networking.apiModel.registrationData.REGISTRATION_DATA
import com.betconnect.networking.apiModel.registrationData.RegistrationData
import com.betconnect.networking.apiModel.registrationData.SportPreferencesListItem
import com.betconnect.networking.apiModel.registrationData.SportsPreferences
import kotlinx.android.synthetic.main.sport_preferences_layout.view.*

class SportPreferencesController(args: Bundle) : BaseController(args), OnBackHandler {

    private lateinit var sportsAdapter: SportPreferencesAdapter

    private val registrationData = args.getParcelable(REGISTRATION_DATA) as RegistrationData

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.sport_preferences_layout, container, false).apply {

            sportPreferencesRv.run {
                layoutManager = GridLayoutManager(context, 2)
                sportsAdapter = SportPreferencesAdapter(
                    SportsPreferences.values().sortedBy { it.displayName }
                        .map {
                            SportPreferencesListItem(
                                it,
                                registrationData.sportsPreferences?.contains(it) ?: true
                            )
                        }
                ) { enable ->
                    this@apply.nextBtn.isEnabled = enable
                }
                adapter = sportsAdapter
            }

            nextBtn.setOnClickListener {
                registrationData.sportsPreferences =
                    sportsAdapter.sportPreferences.filter { it.selected }.map { it.option }
                navigation.show(SignUpStep3Controller.create(registrationData))
            }

            previousBtn.setOnClickListener {
                navigation.onBack()
            }
        }

    companion object {
        fun create(registrationData: RegistrationData) = SportPreferencesController(Bundle().apply {
            putParcelable(REGISTRATION_DATA, registrationData)
        })
    }

    override fun onBack() {
        registrationData.sportsPreferences =
            sportsAdapter.sportPreferences.filter { it.selected }.map { it.option }
        navigation.show(SignUpAccountDetails.create(registrationData), shouldReplace = true)
    }

}
