package com.betconnect.app.vp.onboarding

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.betconnect.app.R
import com.betconnect.app.base.BaseController
import kotlinx.android.synthetic.main.onboarding_step_three.view.*

class OnboardingStep3Controller : BaseController() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.onboarding_step_three, container, false).apply {

            nextBtn.setOnClickListener {
                navigation.show(OnboardingStep4Controller(), shouldReplace = true)
            }

            backBtn.setOnClickListener {
                navigation.show(OnboardingStep2Controller(), shouldReplace = true)
            }

        }

}
