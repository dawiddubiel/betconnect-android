package com.betconnect.app.vp.login.loginSettings

import com.betconnect.app.FingerprintPreferences

interface ILoginSettingsPresenter {
    fun setFingerprintEnabled(enabled: Boolean)
    fun start()
}

class LoginSettingsPresenter(
    private val view: LoginSettingsView,
    private val fingerprintPreferences: FingerprintPreferences): ILoginSettingsPresenter {

    override fun start() {
        view.indicateFingerprintEnabled(fingerprintPreferences.isFingerprintLoginAllowedByUser())
    }

    override fun setFingerprintEnabled(enabled: Boolean) {
       fingerprintPreferences.allowFingerprintLogin(enabled)
    }


}