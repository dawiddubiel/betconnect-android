package com.betconnect.app.vp.signup.step1

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.betconnect.app.R
import com.betconnect.app.base.BaseController
import com.betconnect.app.custom.*
import com.betconnect.app.di.PERSON_TITLES
import com.betconnect.app.navigation.OnBackHandler
import com.betconnect.app.vp.login.LoginController
import com.betconnect.app.vp.signup.step2.SignUpAccountDetails
import com.betconnect.networking.apiModel.registrationData.REGISTRATION_DATA
import com.betconnect.networking.apiModel.registrationData.RegistrationData
import com.betconnect.networking.apiModel.registrationData.UserRole
import kotlinx.android.synthetic.main.sign_up_step_one.view.*
import kotlinx.coroutines.flow.map
import org.koin.core.get
import org.koin.core.inject
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named

private const val TERMS_AND_CONDITIONS_URL = "https://www.betconnect.com/terms-conditions"

interface SignUpStep1View {
    fun onInputsValid(valid: Boolean)
    fun initViews(registrationData: RegistrationData)
    fun proceedToNextStep(registrationData: RegistrationData)
}

class SignUpStep1Controller(args: Bundle? = null) : BaseController(args), SignUpStep1View,
    OnBackHandler {

    private val presenter: ISignUpStep1Presenter by inject {
        parametersOf(
            this,
            (args?.getParcelable(REGISTRATION_DATA) as? RegistrationData ?: RegistrationData())
        )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.sign_up_step_one, container, false).apply {
            titlesSpinner.adapter = ArrayAdapter<String>(context, R.layout.spinner_item).apply {
                addAll(get<List<String>>(named(PERSON_TITLES)))
                setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            }
            tcCheckBox.setClickableSpans(
                stringId = R.string.agree_tc,
                highlightColorResId = R.color.textColorBlue,
                underline = false,
                actions = ClickableTextViewSpans().apply {
                    put(resources.getString(R.string.tc)) {
                        startActivity(
                            Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse(TERMS_AND_CONDITIONS_URL)
                            )
                        )
                    }
                })
            nextBtn.setOnClickListener {
                presenter.nextClicked()
            }
            backBtn.setOnClickListener { navigation.onBack() }
            presenter.consumeInputs(
                titleFlow = titlesSpinner.itemSelectionsFlow(),
                firstNameFlow = firstNameEt.textChangedFlow(),
                lastNameFlow = lastNameEt.textChangedFlow(),
                emailFlow = emailEt.textChangedFlow(),
                accountTypeFlow = accountTypeToggle.getSelectionFlow().map { UserRole.valOf(it) },
                termsAcceptedFlow = tcCheckBox.checkChangedFlow(),
                marketingAcceptedFlow = marketingCheckBox.checkChangedFlow()
            )
        }

    override fun onAttach(view: View) {
        super.onAttach(view)
        presenter.start()
    }

    override fun onInputsValid(valid: Boolean) {
        view?.nextBtn?.isEnabled = valid
    }

    override fun onDestroy() {
        presenter.destroy()
        super.onDestroy()
    }

    override fun initViews(registrationData: RegistrationData) {
        view?.apply {
            registrationData.run {
                title?.let {
                    titlesSpinner.setSelection(
                        get<List<String>>(named(PERSON_TITLES)).indexOf(it) + 1
                    )
                }
                firstname?.let { firstNameEt.setText(it) }
                lastname?.let { lastNameEt.setText(it) }
                email?.let { emailEt.setText(it) }
                termsAccepted?.let { tcCheckBox.isChecked = it }
                marketingTermsAccepted?.let { marketingCheckBox.isChecked = it }
                role?.let { accountTypeToggle.setSelection(it.value) }
            }
        }
    }

    override fun proceedToNextStep(registrationData: RegistrationData) {
        navigation.show(SignUpAccountDetails.create(registrationData), shouldReplace = true)
    }

    companion object {
        fun create(registrationData: RegistrationData) = SignUpStep1Controller(Bundle().apply {
            putParcelable(REGISTRATION_DATA, registrationData)
        })
    }

    override fun onBack() {
        navigation.show(LoginController(), shouldReplace = true)
    }

}
