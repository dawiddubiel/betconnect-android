package com.betconnect.app.vp.webView

import android.graphics.Bitmap
import android.net.http.SslError
import android.webkit.*
import android.webkit.WebView

class MainWebView(private val onShouldInterceptRequestAction: ((WebResourceRequest?) -> Unit)? = null,
                  private val onPageStartedAction: ((String?) -> Unit)? = null,
                  private val onShouldOverrideUrlLoading: ((String?) -> Boolean)? = null,
                  private val onPageFinishedAction: (() -> Unit)? = null) : WebViewClient() {

    override fun shouldInterceptRequest(view: WebView?, request: WebResourceRequest?): WebResourceResponse? {
        onShouldInterceptRequestAction?.invoke(request)
        return super.shouldInterceptRequest(view, request)
    }

    override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
        if (onShouldOverrideUrlLoading?.invoke(url) == true) view.loadUrl(url)
        return true
    }

    override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
        onPageStartedAction?.invoke(url)
        super.onPageStarted(view, url, favicon)
    }

    override fun onPageFinished(view: WebView?, url: String?) {
        super.onPageFinished(view, url)
        onPageFinishedAction?.invoke()
    }

    override fun onReceivedSslError(view: WebView?, handler: SslErrorHandler?, error: SslError?) {
        handler?.proceed()
    }

}
