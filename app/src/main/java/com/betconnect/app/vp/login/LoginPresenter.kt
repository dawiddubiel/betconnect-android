package com.betconnect.app.vp.login

import android.webkit.CookieManager
import com.betconnect.app.FingerprintPreferences
import com.betconnect.app.base.BasePresenter
import com.betconnect.app.base.IBasePresenter
import com.betconnect.app.biometricAuth.Credentials
import com.betconnect.app.biometricAuth.isAtLeastApi23
import com.betconnect.app.fingerprint.ICredentialsRepo
import com.betconnect.networking.LOCATION_FAILED
import com.betconnect.networking.LOGIN_FAILED
import com.betconnect.networking.RemoteDataSource
import com.betconnect.networking.apiModel.LoginData
import com.betconnect.networking.apiModel.registrationData.UserRole
import com.betconnect.networking.customerIO.ANDROID_PLATFORM_NAME
import com.betconnect.networking.customerIO.model.Device
import com.betconnect.networking.customerIO.model.DeviceInfo
import com.betconnect.networking.di.BASE_URL
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper

interface ILoginPresenter : IBasePresenter {
    fun attemptLogin(login: String, password: String, withFingerprint: Boolean)
    fun isFingerprintLoginAvailable(): Boolean
}

class LoginPresenter(
    private val view: LoginView,
    private val cookieManager: CookieManager,
    private val fingerprintPreferences: FingerprintPreferences,
    private val credentialsRepo: ICredentialsRepo,
    private val remoteDataSource: RemoteDataSource
) : ILoginPresenter, BasePresenter() {

    override fun start() {
        view.enableLoginSettings(fingerprintPreferences.isFingerprintSettingSet())
        processFingerprint()
    }

    override fun attemptLogin(
        login: String,
        password: String,
        withFingerprint: Boolean
    ) {
        view.run {
            invalidateErrors()
            validateCredentials(login, password) {
                lockAndLoad {
                    val firebaseId = remoteDataSource.getFirebaseId()
                    remoteDataSource.login(
                        LoginData(
                            login,
                            password,
                            firebaseId ?: ""
                        )
                    )
                        .onSuccess { loginResponse ->
                            cookieManager.setCookie(BASE_URL, loginResponse.cookie)
                            firebaseId?.let { firebaseId ->
                                remoteDataSource.pushDeviceInfo(
                                    login,
                                    device = Device(
                                        DeviceInfo(
                                            id = firebaseId,
                                            platform = ANDROID_PLATFORM_NAME,
                                            last_used = System.currentTimeMillis() / 1000
                                        )
                                    )
                                )
                            }
                            view.onLoginSuccess(
                                UserRole.valOf(loginResponse.userRole)!!,
                                composeCredentialsParam(login, password)
                            )
                        }
                        .onError {
                            when (it.message) {
                                LOGIN_FAILED -> {
                                    showLoginFailedError()
                                    if (withFingerprint) {
                                        invalidateStoredCredentials()
                                    }
                                }
                                LOCATION_FAILED -> {
                                    onLocationUnavailable()
                                }
                            }
                        }
                }
            }
        }
    }

    private fun invalidateStoredCredentials() {
        fingerprintPreferences.resetAllowFingerprintLogin()
        credentialsRepo.clearCredentialsData()
    }

    private fun composeCredentialsParam(
        email: String,
        password: String
    ): String? {
        return if (isAtLeastApi23())
            jacksonObjectMapper().writeValueAsString(Credentials(email, password))
        else null
    }


    override fun isFingerprintLoginAvailable() =
        fingerprintPreferences.isFingerprintLoginAllowedByUser() && !fingerprintPreferences.isFingerprintBlocked() && credentialsRepo.getCredentials() != null

    private fun LoginView.validateCredentials(
        login: String,
        password: String,
        onSuccess: () -> Unit
    ) {
        when {
            login.isBlank() && password.isBlank() -> {
                showEmptyPassword()
                showEmptyLogin()
            }
            login.isBlank() -> showEmptyLogin()
            password.isBlank() -> showEmptyPassword()
            else -> onSuccess()
        }
    }

    private fun processFingerprint() {
        if (!fingerprintPreferences.isFingerprintBlocked() && fingerprintPreferences.isFingerprintLoginAllowedByUser() && credentialsRepo.getCredentials() != null
        ) {
            view.showBiometricPrompt()
            /*CryptoTask(cryptoTaskType = CryptoTaskType.Decryption,
                string = fingerprintPreferences.getCredentialsEncrypted()!!,
                onTaskStart = { view.showProgress(true) },
                onSuccess = { credentials ->
                    credentials?.let {
                        jacksonObjectMapper().readValue(
                            credentials,
                            Credentials::class.java
                        )
                            ?.run {
                                attemptLogin(
                                    login,
                                    password,
                                    true
                                )
                            }
                    } ?: run {
                        fingerprintPreferences.clearCredentialsData()
                        view.showProgress(false)
                    }
                }, onError = {
                    fingerprintPreferences.setFingerprintBlocked(true)
                    view.showProgress(false)
                })*/
        }
    }

}
