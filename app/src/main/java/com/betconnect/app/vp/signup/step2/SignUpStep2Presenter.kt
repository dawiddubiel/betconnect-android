package com.betconnect.app.vp.signup.step2

import com.betconnect.app.base.BasePresenter
import com.betconnect.app.base.IBasePresenter
import com.betconnect.app.custom.MaxLiabilityPresentation
import com.betconnect.app.extensions.areOnlyLettersAndNumbers
import com.betconnect.networking.apiModel.registrationData.BirthDate
import com.betconnect.networking.apiModel.registrationData.RegistrationData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import java.util.*

interface ISignUpStep2Presenter : IBasePresenter {
    fun onNext()
    fun getRegistrationData(): RegistrationData
    fun consumeInputs(
        handleFlow: Flow<String>,
        passwordFlow: Flow<String>,
        confirmPasswordFlow: Flow<String>,
        birthDateFlow: Flow<BirthDate?>,
        maxLiabilityFlow: Flow<MaxLiabilityPresentation?>
    )

    fun onBirthDateSelected(day: Int, month: Int, year: Int)
}

class SignUpStep2Presenter(
    private val view: SignUpStep2View,
    private val registrationData: RegistrationData
) : ISignUpStep2Presenter, BasePresenter() {

    override fun start() {
        super.start()
        view.fillOutViews(registrationData)
    }

    private fun validateInputs(
        handle: String,
        password: String,
        confirmPassword: String,
        birthDate: BirthDate?,
        maxLiability: MaxLiabilityPresentation?
    ) {
        registrationData.apply {
            this.handle = handle
            this.password = password
            this.passwordConfirm = confirmPassword
            this.birthdate = birthDate
            this.maxLiabilityInCents = maxLiability?.value
        }
        view.apply {
            showPasswordToShortWarning(password.isNotBlank() && password.length < 8)
            showConfirmPasswordNotMatchWarning(confirmPassword.isNotEmpty() && confirmPassword != password)
            registrationData.birthdate?.let {
                showUserToYoungWarning(!isBirthDateValid(it))
            }
            showUsernameInvalidWarning((handle.isNotEmpty() && handle.length < 3) || !handle.areOnlyLettersAndNumbers())
            onInputsValid(
                isHandleValid(handle) && isPasswordValid(
                    password,
                    confirmPassword
                ) && isBirthDateValid(registrationData.birthdate) && maxLiability != null
            )
        }
    }

    override fun onNext() {
        view.proceedToNextStep(registrationData)
    }

    override fun getRegistrationData() = registrationData

    override fun consumeInputs(
        handleFlow: Flow<String>,
        passwordFlow: Flow<String>,
        confirmPasswordFlow: Flow<String>,
        birthDateFlow: Flow<BirthDate?>,
        maxLiabilityFlow: Flow<MaxLiabilityPresentation?>
    ) {
        combine(
            handleFlow,
            passwordFlow,
            confirmPasswordFlow,
            birthDateFlow,
            maxLiabilityFlow
        ) { handle, password, confirmPassword, birthDate, maxLiability ->
            validateInputs(handle, password, confirmPassword, birthDate, maxLiability)
        }.launchIn(this)
    }

    override fun onBirthDateSelected(day: Int, month: Int, year: Int) {
        registrationData.birthdate = BirthDate(year, month, day)
        view.showUserToYoungWarning(!isBirthDateValid(registrationData.birthdate))
    }

    private fun isHandleValid(handle: String): Boolean =
        handle.length >= 3 && handle.areOnlyLettersAndNumbers()

    private fun isPasswordValid(password: String, confirmPassword: String) =
        password.length >= 8 && confirmPassword == password

    private fun isBirthDateValid(birthDate: BirthDate?) = birthDate?.let {
        val minAdultAge = GregorianCalendar()
        minAdultAge.add(Calendar.YEAR, -18)
        if (minAdultAge.before(
                GregorianCalendar(
                    birthDate.yearOfBirth,
                    birthDate.monthOfBirth,
                    birthDate.dayOfBirth
                )
            )
        ) {
            return false
        }
        return true
    } ?: false

}
