package com.betconnect.app.vp

import android.Manifest
import android.content.Context
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricPrompt
import com.betconnect.app.R
import com.betconnect.app.di.MAIN_ACTIVITY_SCOPE
import com.betconnect.app.di.MAIN_NAVIGATION
import com.betconnect.app.extensions.makeGone
import com.betconnect.app.extensions.makeVisible
import com.betconnect.app.navigation.BiometricPromptHost
import com.betconnect.app.navigation.MainViewHost
import com.betconnect.app.navigation.Navigation
import com.betconnect.app.navigation.NavigationParams
import com.betconnect.app.vp.fingerprint.FingerprintQuestionController
import com.betconnect.app.vp.geolocation.EnableLocationController
import com.betconnect.app.vp.geolocation.LocationBlockedController
import com.betconnect.app.vp.geolocation.UnableToLocalizeController
import com.betconnect.app.vp.login.LoginController
import com.betconnect.app.vp.onboarding.OnboardingStep1Controller
import com.bluelinelabs.conductor.Conductor
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.get
import org.koin.android.ext.android.getKoin
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions


interface IMainActivity {
    fun startOnboarding()
    fun showUnallowedLocationWarning()
    fun onGpsDisabled()
    fun onLocationDisabled()
    fun onAllowedLocalization()
    fun showLocationPermissionRationale()
}

@RuntimePermissions
class MainActivity : AppCompatActivity(), IMainActivity, MainViewHost,
    BiometricPromptHost {

    private val presenter by inject<MainActivityPresenter> { parametersOf(this) }
    private val biometricPrompt by inject<BiometricPrompt> {
        parametersOf(
            this,
            {
                navigation.showProgress(true)
            }
            ,
            { decryptedCredentials: String? ->
                navigation.onCredentialsDecrypted(decryptedCredentials)
            }
            ,
            { navigation.showProgress(false) })
    }

    private val navigation: Navigation by lazy {
        getKoin().getScope(MAIN_ACTIVITY_SCOPE).get<Navigation>(named(MAIN_NAVIGATION)) {
            parametersOf(
                NavigationParams(
                    Conductor.attachRouter(this@MainActivity, mainLoginViewController, null),
                    Conductor.attachRouter(this@MainActivity, dialogController, null),
                    Conductor.attachRouter(this@MainActivity, lockViewLt, null),
                    dialogController,
                    lockViewLt,
                    this@MainActivity,
                    this@MainActivity
                )
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getKoin().getOrCreateScope(MAIN_ACTIVITY_SCOPE, named(MAIN_ACTIVITY_SCOPE))
        presenter.start()
        navigation.show(LoginController())
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN and WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
    }

    override fun onBackPressed() {
        navigation.onBack()
    }

    override fun onResume() {
        super.onResume()
        presenter.processLocation()
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

    fun onOnboardingComplete() {
        presenter.setOnboardingCompleted()
    }

    override fun startOnboarding() {
        runOnUiThread {
            dialogController.makeVisible()
            navigation.show(OnboardingStep1Controller())
        }
    }

    override fun showProgress(show: Boolean) {
        runOnUiThread { if (show) progress.makeVisible() else progress.makeGone() }
    }

    override fun onDestroy() {
        getKoin().deleteScope(MAIN_ACTIVITY_SCOPE)
        presenter.stop()
        super.onDestroy()
    }


    override fun showUnallowedLocationWarning() {
        navigation.show(LocationBlockedController())
    }

    override fun onGpsDisabled() {
        navigation.lockScreen(UnableToLocalizeController())
    }

    override fun onAllowedLocalization() {
        navigation.unlockScreen()
    }

    override fun onStop() {
        showBiometricPrompt(false)
        super.onStop()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    @NeedsPermission(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )
    fun verifyLocation() {
        presenter.startMonitoringLocation()
    }

    @OnPermissionDenied(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )
    fun onLocationPermissionDenied() {
        presenter.onUserLocationPermissionDenied()
    }

    @OnNeverAskAgain(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )
    fun onLocationPermissionDeniedPermanently() {
        presenter.onUserLocationPermissionDenied()
    }

    fun showFingerprintQuestion(param: String) {
        if (presenter.shouldShowFingerprintQuestion()) {
            navigation.show(FingerprintQuestionController.create(param))
        }
    }

    override fun showBiometricPrompt(show: Boolean) {
        if (show) {
            biometricPrompt.authenticate(get())
        } else {
            biometricPrompt.cancelAuthentication()
        }
    }

    override fun onLocationDisabled() {
        navigation.lockScreen(EnableLocationController())
    }

    override fun showLocationPermissionRationale() {
        verifyLocationWithPermissionCheck()
    }

}
