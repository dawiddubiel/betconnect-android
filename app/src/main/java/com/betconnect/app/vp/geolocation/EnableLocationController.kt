package com.betconnect.app.vp.geolocation

import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.betconnect.app.R
import com.betconnect.app.base.BaseController
import com.betconnect.app.navigation.LockScreen
import com.betconnect.app.navigation.OnBackHandler
import kotlinx.android.synthetic.main.enable_location_layout.view.*

class EnableLocationController : BaseController(), LockScreen, OnBackHandler {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.enable_location_layout, container, false).apply {
            updateSettingsBtn.setOnClickListener {
                startActivity(Intent().apply {
                    action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    data = Uri.fromParts("package", activity!!.packageName, null)
                })
            }
        }

    override fun onBack() {
        activity?.moveTaskToBack(false)
    }

}
