package com.betconnect.app.vp.signup.addressLookup

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.betconnect.app.R
import com.betconnect.networking.address.AddressResult
import kotlinx.android.synthetic.main.address_lookup_item.view.*

class AddressLookupAdapter(
    userList: List<AddressResult>,
    private val onItemClick: (AddressResult) -> Unit
) :
    RecyclerView.Adapter<AddressLookupAdapter.ViewHolder>() {

    private var dataset = userList

    fun setData(addresses: List<AddressResult>) {
        dataset = addresses
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(dataset[position], onItemClick)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.address_lookup_item,
                parent,
                false
            )
        )

    override fun getItemCount() = dataset.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(
            addressResult: AddressResult,
            onItemClick: (AddressResult) -> Unit
        ) {
            itemView.address_tv.apply {
                text = addressResult.toString()
                setOnClickListener { onItemClick(addressResult) }
            }
        }
    }

}
