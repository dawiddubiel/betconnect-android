package com.betconnect.app.vp.webView

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import com.betconnect.app.R
import com.betconnect.app.base.BaseController
import com.betconnect.app.vp.MainActivity
import kotlinx.android.synthetic.main.support_webview_layout.view.*

const val URL = "SupportWebViewUrl"

class SupportWebViewController(args: Bundle? = null) : BaseController(args) {

    private var pageFinished = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.support_webview_layout, null, false).apply {
            (activity as? MainActivity)?.showProgress(true)
            configureWebView()
            webView.loadUrl(args.getString(URL))
            goBack.setOnClickListener {
                navigation.onBack()
            }

        }

    @SuppressLint("SetJavaScriptEnabled")
    private fun configureWebView() {
        view?.webView?.apply {
            webViewClient = MainWebView(
                onShouldInterceptRequestAction = { true },
                onPageFinishedAction = {
                    pageFinished = true
                    navigation.showProgress(false)
                }
            )
            webChromeClient = WebChromeClient()
            settings.apply {
                javaScriptEnabled = true
                domStorageEnabled = true
                userAgentString = "betconnect-android-wrapper-app"
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                settings.safeBrowsingEnabled = false
            }
        }
    }

}
