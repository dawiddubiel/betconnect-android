package com.betconnect.app.vp.webView

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.widget.TextView
import android.widget.Toast
import com.betconnect.app.R
import com.betconnect.app.base.BaseController
import com.betconnect.app.di.EXTERNAL_URLS
import com.betconnect.app.extensions.makeInvisible
import com.betconnect.app.extensions.makeVisible
import com.betconnect.app.extensions.removePaddingFromNavigationItems
import com.betconnect.app.extensions.runOnUiThread
import com.betconnect.app.navigation.OnBackHandler
import com.betconnect.app.vp.login.LoginController
import com.betconnect.networking.apiModel.registrationData.UserRole
import com.betconnect.networking.di.BASE_URL
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView
import io.github.inflationx.calligraphy3.CalligraphyUtils
import kotlinx.android.synthetic.main.webview.view.*
import org.koin.core.inject
import org.koin.core.qualifier.named

const val PRO_START_URL = "$BASE_URL/bets/new"
const val PUNTER_START_URL = "$BASE_URL/bets/incoming"

private const val EXIT_TIMEOUT: Long = 2000

@Suppress("OverridingDeprecatedMember")
class WebViewController(args: Bundle? = null) : BaseController(args), OnBackHandler {

    private var pageFinished = false
    private var lastRequest: String? = null
    private var lastBackPressTime = 0L

    private val requestsToIntercept: List<String> by inject(named(EXTERNAL_URLS))

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.webview, null, false).apply {
            navigation.showProgress(true)
            configureWebView(webView)
            when (args.getParcelable<UserRole>(USER_ROLE)!!) {
                UserRole.Punter -> {
                    navigationViewPro.makeInvisible()
                    navigationView.makeVisible()
                    webView.loadUrl(PUNTER_START_URL)
                }
                UserRole.Pro -> {
                    navigationViewPro.makeVisible()
                    navigationView.makeInvisible()
                    webView.loadUrl(PRO_START_URL)
                }
            }
            navigationView.setOnNavigationItemSelectedListener(punterNavigation)
            navigationViewPro.setOnNavigationItemSelectedListener(proNavigation)

            (navigationView.getChildAt(0) as BottomNavigationMenuView).removePaddingFromNavigationItems()
            (navigationViewPro.getChildAt(0) as BottomNavigationMenuView).removePaddingFromNavigationItems()
            applyBottomNavFont(navigationView)
            applyBottomNavFont(navigationViewPro)
        }

    @SuppressLint("SetJavaScriptEnabled")
    private fun configureWebView(webView: WebView) {
        webView.apply {
            webViewClient = MainWebView(
                onShouldInterceptRequestAction = { request ->
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        val newRequest = request!!.url.toString()
                        if (newRequest.endsWith("logout") && pageFinished) {
                            runOnUiThread {
                              // destroyWebView()
                                navigation.show(LoginController(), shouldReplace = true)
                            }
                        }
                        if (requestsToIntercept.any { it.contains(newRequest) } && pageFinished) {
                            runOnUiThread {
                                navigation.show(SupportWebViewController(Bundle().apply {
                                    putString(URL, newRequest)
                                }))
                            }
                        }
                        lastRequest = newRequest
                    }
                },
                onShouldOverrideUrlLoading = { url ->
                    url?.let { !it.contains("login") } ?: true
                },
                onPageFinishedAction = {
                    pageFinished = true
                    navigation.showProgress(false)
                }
            )
            webChromeClient = WebChromeClient()
            settings.apply {
                javaScriptCanOpenWindowsAutomatically = true
                setAppCacheEnabled(true)
                javaScriptEnabled = true
                databaseEnabled = true
                domStorageEnabled = true
                userAgentString = "betconnect-android-wrapper-app"
            }
        }
    }

    override fun onBack() {
        view?.run {
            when {
                webView.canGoBack() -> webView.goBack()
                System.currentTimeMillis() - lastBackPressTime < EXIT_TIMEOUT -> activity?.moveTaskToBack(
                    true
                )

                else -> {
                    Toast.makeText(
                        context,
                        context.getString(R.string.press_once_more_to_exit),
                        Toast.LENGTH_SHORT
                    ).show()
                    lastBackPressTime = System.currentTimeMillis()
                }
            }
        }
    }

    private val punterNavigation = object : BottomNavigationView.OnNavigationItemSelectedListener {

        override fun onNavigationItemSelected(item: MenuItem): Boolean {
            when (item.itemId) {
                R.id.navigation_my_bets -> {
                    view?.webView?.loadUrl("$BASE_URL/bets/incoming")
                    return true
                }
                R.id.navigation_pros -> {
                    view?.webView?.loadUrl("$BASE_URL/pros/list")
                    return true
                }
                R.id.navigation_transactions -> {
                    view?.webView?.loadUrl("$BASE_URL/transactions")
                    return true
                }
                R.id.navigation_profile -> {
                    view?.webView?.loadUrl("$BASE_URL/punter/profile")
                    return true
                }
                R.id.navigation_settings -> {
                    view?.webView?.loadUrl("$BASE_URL/account")
                    return true
                }
            }
            return false
        }
    }

    private val proNavigation = object : BottomNavigationView.OnNavigationItemSelectedListener {

        override fun onNavigationItemSelected(item: MenuItem): Boolean {
            when (item.itemId) {
                R.id.navigation_my_bets -> {
                    view?.webView?.loadUrl("$BASE_URL/bets/new")
                    return true
                }
                R.id.navigation_transactions -> {
                    view?.webView?.loadUrl("$BASE_URL/transactions")
                    return true
                }
                R.id.navigation_profile -> {
                    view?.webView?.loadUrl("$BASE_URL/pro/profile")
                    return true
                }
                R.id.navigation_settings -> {
                    view?.webView?.loadUrl("$BASE_URL/account")
                    return true
                }
            }
            return false
        }
    }

    private fun destroyWebView() {
        view?.run {
            webView.clearCache(true)
            webView.loadUrl("about:blank")
            webView.onPause()
            webView.removeAllViews()
            webView.destroy()
        }
    }

    private fun applyBottomNavFont(mBottomNav: BottomNavigationView) {
        for (i in 0 until mBottomNav.childCount) {
            val child = mBottomNav.getChildAt(i)
            if (child is BottomNavigationMenuView) {
                val menu = child
                for (j in 0 until menu.childCount) {
                    val item = menu.getChildAt(j)
                    val smallItemText = item.findViewById<View>(R.id.smallLabel)
                    if (smallItemText is TextView) {
                        CalligraphyUtils.applyFontToTextView(
                            smallItemText.context,
                            smallItemText,
                            "fonts/Proxima-Nova-Regular.ttf"
                        )
                    }
                    val largeItemText = item.findViewById<View>(R.id.largeLabel)
                    if (largeItemText is TextView) {
                        CalligraphyUtils.applyFontToTextView(
                            largeItemText.context,
                            largeItemText,
                            "fonts/Proxima-Nova-Regular.ttf"
                        )
                    }
                }
            }
        }
    }

    companion object {
        private const val USER_ROLE = "USER_ROLE"
        fun create(userRole: UserRole) = WebViewController(Bundle().apply {
            putParcelable(USER_ROLE, userRole)
        })
    }

}
