package com.betconnect.app.vp.forgotPassword

import com.betconnect.app.base.BasePresenter
import com.betconnect.app.base.IBasePresenter
import com.betconnect.networking.RemoteDataSource
import com.betconnect.networking.apiModel.ResetPasswordData

interface IForgotPasswordPresenter: IBasePresenter {
    fun resetPassword(email: String)
}

class ForgotPasswordPresenter(
    private val view: ForgotPasswordView,
    private val remoteDataSource: RemoteDataSource
) : IForgotPasswordPresenter, BasePresenter() {

    override fun resetPassword(email: String) {
        view.lockAndLoad {
            remoteDataSource.forgotPassword(ResetPasswordData(email))
                .onSuccess { view.onPasswordResetSuccess() }
                .onError { view.onPasswordResetError(it.message) }
        }
    }

}
