package com.betconnect.app.vp.fingerprint

import android.os.Build
import androidx.annotation.RequiresApi
import com.betconnect.app.FingerprintPreferences
import com.betconnect.app.base.BasePresenter
import com.betconnect.app.fingerprint.ICredentialsRepo

interface IFingerprintQuestionPresenter {
    fun turnOnFingerprint()
    fun notNow()
}

@RequiresApi(Build.VERSION_CODES.M)
class FingerprintQuestionPresenter(
    private val view: FingerprintQuestionView,
    credentials: String,
    private val preferences: FingerprintPreferences,
    credentialsRepo: ICredentialsRepo
) : BasePresenter(), IFingerprintQuestionPresenter {

    init {
        credentialsRepo.setCredentials(credentials)
    }

    override fun turnOnFingerprint() {
        preferences.allowFingerprintLogin(true)
        view.onQuestionAnswered()
    }

    override fun notNow() {
        preferences.allowFingerprintLogin(false)
        view.onQuestionAnswered()
    }

}