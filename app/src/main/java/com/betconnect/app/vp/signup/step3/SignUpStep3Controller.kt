package com.betconnect.app.vp.signup.step3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.betconnect.app.IBaseController
import com.betconnect.app.R
import com.betconnect.app.base.BaseController
import com.betconnect.app.custom.itemSelectionsFlow
import com.betconnect.app.custom.textChangedFlow
import com.betconnect.app.dialog.InfoDialogController
import com.betconnect.app.extensions.Direction
import com.betconnect.app.extensions.toggleVisibility
import com.betconnect.app.navigation.OnBackHandler
import com.betconnect.app.vp.geolocation.LocationBlockedController
import com.betconnect.app.vp.signup.addressLookup.AddressLookupController
import com.betconnect.app.vp.signup.sport_preferences.SportPreferencesController
import com.betconnect.app.vp.signup.step2.SignUpAccountDetails
import com.betconnect.app.vp.webView.WebViewController
import com.betconnect.networking.apiModel.registrationData.*
import kotlinx.android.synthetic.main.edit_text_with_prefix.view.*
import kotlinx.android.synthetic.main.sign_up_contact_details.view.*
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import org.koin.core.inject
import org.koin.core.parameter.parametersOf

interface SignUpStep3View : IBaseController {
    fun onInputsValid(valid: Boolean)
    fun showWebView(userRole: UserRole)
    fun showSignUpError(message: String)
    fun showLocationRestrictedException()
    fun fillOutViews(registrationData: RegistrationData)
    fun showLimitAmountView(show: Boolean)
}

class SignUpStep3Controller(args: Bundle) : BaseController(args), SignUpStep3View, OnBackHandler {

    private val presenter: ISignUpStep3Presenter by inject {
        parametersOf(this, args.getParcelable(REGISTRATION_DATA))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.sign_up_contact_details, container, false).apply {

            limitDurationSpinner.adapter = ArrayAdapter(
                inflater.context, R.layout.spinner_item, EDepositLimitDuration.values()
            ).apply { setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item) }

            previousBtn.setOnClickListener { navigation.onBack() }

            addressTv.setOnClickListener {
                navigation.show(
                    AddressLookupController.create(presenter.getRegistrationData()),
                    shouldReplace = true
                )
            }

            createAccountBtn.setOnClickListener { presenter.createAccount() }

            presenter.consumeInputs(
                phoneNumberET.textChangedFlow(true),
                depositLimitAmountView.editText.textChangedFlow(true),
                limitDurationSpinner.itemSelectionsFlow()
            )
        }

    override fun onAttach(view: View) {
        super.onAttach(view)
        presenter.start()
    }

    override fun onDestroy() {
        presenter.destroy()
        super.onDestroy()
    }

    override fun fillOutViews(registrationData: RegistrationData) {
        view?.apply {
            phoneNumberET.setText(registrationData.contactNumber)
            registrationData.run {
                userAddress.postalCode?.let {
                    addressTv.text = registrationData.userAddress.getStringValue()
                }
                depositLimitAmount?.let { depositLimitAmountView.editText.setText((it / 100).toString()) }
                depositLimitDuration?.let {
                    when (it) {
                        EDepositLimitDuration.DAY -> limitDurationSpinner.setSelection(0)
                        EDepositLimitDuration.WEEK -> limitDurationSpinner.setSelection(1)
                        EDepositLimitDuration.MONTH -> limitDurationSpinner.setSelection(2)
                        EDepositLimitDuration.UNLIMITED -> limitDurationSpinner.setSelection(3)
                    }
                }
            }
        }
    }

    override fun showLimitAmountView(show: Boolean) {
        view?.depositLimitAmountView?.toggleVisibility(show)
    }

    override fun onInputsValid(valid: Boolean) {
        view?.createAccountBtn?.isEnabled = valid
    }

    override fun showWebView(userRole: UserRole) {
        navigation.show(WebViewController.create(userRole = userRole))
    }

    override fun showSignUpError(message: String) {
        navigation.show(InfoDialogController.create(message))
    }

    override fun showLocationRestrictedException() {
        navigation.show(LocationBlockedController())
    }

    companion object {
        fun create(registrationData: RegistrationData) = SignUpStep3Controller(Bundle().apply {
            putParcelable(REGISTRATION_DATA, registrationData)
        })
    }

    override fun onBack() {
        presenter.getRegistrationData().run {
            navigation.show(
                if (role != UserRole.Pro) SportPreferencesController.create(this) else SignUpAccountDetails.create(
                    this
                ),
                shouldReplace = true,
                direction = Direction.Horizontal
            )
        }
    }

}
