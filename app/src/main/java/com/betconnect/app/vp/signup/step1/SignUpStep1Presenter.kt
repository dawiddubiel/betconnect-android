package com.betconnect.app.vp.signup.step1

import com.betconnect.app.base.BasePresenter
import com.betconnect.app.base.IBasePresenter
import com.betconnect.app.extensions.isEmailValid
import com.betconnect.networking.apiModel.registrationData.RegistrationData
import com.betconnect.networking.apiModel.registrationData.UserRole
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn

interface ISignUpStep1Presenter : IBasePresenter {
    fun nextClicked()
    fun consumeInputs(
        titleFlow: Flow<String?>,
        firstNameFlow: Flow<String>,
        lastNameFlow: Flow<String>,
        emailFlow: Flow<String>,
        accountTypeFlow: Flow<UserRole?>,
        termsAcceptedFlow: Flow<Boolean>,
        marketingAcceptedFlow: Flow<Boolean>
    )
}

class SignUpStep1Presenter(
    private val view: SignUpStep1View,
    private var registrationData: RegistrationData
) : ISignUpStep1Presenter, BasePresenter() {

    override fun consumeInputs(
        titleFlow: Flow<String?>,
        firstNameFlow: Flow<String>,
        lastNameFlow: Flow<String>,
        emailFlow: Flow<String>,
        accountTypeFlow: Flow<UserRole?>,
        termsAcceptedFlow: Flow<Boolean>,
        marketingAcceptedFlow: Flow<Boolean>
    ) {
        combine(
            titleFlow,
            firstNameFlow,
            lastNameFlow,
            emailFlow,
            accountTypeFlow,
            termsAcceptedFlow,
            marketingAcceptedFlow
        ) {
            validateInputs(
                title = it[0] as? String,
                firstName = it[1] as String,
                lastName = it[2] as String,
                email = it[3] as String,
                accountType = it[4] as UserRole?,
                tcselected = it[5] as Boolean,
                marketingAccepted = it[6] as Boolean
            )
        }
            .launchIn(this)
    }

    override fun start() {
        super.start()
        view.initViews(registrationData)
    }

    private fun validateInputs(
        title: String?,
        firstName: String,
        lastName: String,
        email: String,
        accountType: UserRole?,
        tcselected: Boolean,
        marketingAccepted: Boolean
    ) {
        if (isFirstNameValid(firstName) && isLastNameValid(lastName) && isEmailValid(email) && tcselected && title?.isNotEmpty() == true && accountType != null) {
            view.onInputsValid(true)
            registrationData.apply {
                this.title = title
                this.firstname = firstName
                this.lastname = lastName
                this.email = email.trim()
                this.role = accountType
                this.termsAccepted = tcselected
                this.marketingTermsAccepted = marketingAccepted
            }
        } else {
            view.onInputsValid(false)
        }
    }

    private fun isLastNameValid(lastName: String) = lastName.isNotEmpty()

    private fun isFirstNameValid(firstName: String) = firstName.isNotEmpty()

    override fun nextClicked() {
        view.proceedToNextStep(registrationData)
    }

}
