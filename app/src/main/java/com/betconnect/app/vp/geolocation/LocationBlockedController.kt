package com.betconnect.app.vp.geolocation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.betconnect.app.R
import com.betconnect.app.base.BaseController
import com.betconnect.app.navigation.OnBackHandler

class LocationBlockedController : BaseController(), OnBackHandler {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.location_blocked_view, container, false)

    override fun onBack() {
        activity?.moveTaskToBack(false)
    }

}
