package com.betconnect.app.vp.onboarding

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.betconnect.app.R
import com.betconnect.app.base.BaseController
import com.betconnect.app.vp.MainActivity
import kotlinx.android.synthetic.main.onboarding_step_four.view.*

class OnboardingStep4Controller : BaseController() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
        inflater.inflate(R.layout.onboarding_step_four, container, false).apply {

            nextBtn.setOnClickListener {
                router.popCurrentController()
                (activity as? MainActivity)?.onOnboardingComplete()
            }

            backBtn.setOnClickListener {
                navigation.show(OnboardingStep3Controller(), shouldReplace = true)
            }

        }

}