package com.betconnect.app.geolocation


import android.location.Geocoder
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import java.io.IOException

interface IGeolocationManager {
    val allowedLocationChannel: ReceiveChannel<Boolean>
    fun requestLocationUpdates()
    fun stopLocationUpdates()
}

class GeolocationManager(
    private val fusedLocationProviderClient: FusedLocationProviderClient,
    private val geocoder: Geocoder,
    private val allowedLocations: List<String>
) : IGeolocationManager, LocationCallback() {

    private val _allowedLocationChannel = BroadcastChannel<Boolean>(Channel.CONFLATED).apply {
        invokeOnClose {
            fusedLocationProviderClient.removeLocationUpdates(this@GeolocationManager)
        }
    }
    override val allowedLocationChannel: ReceiveChannel<Boolean> = _allowedLocationChannel.openSubscription()

    override fun requestLocationUpdates() {
        try {
            fusedLocationProviderClient.requestLocationUpdates(
                LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                    .setInterval(10_000), this, null
            )
        } catch (e: SecurityException) {
            _allowedLocationChannel.offer(false)
        }
    }

    override fun onLocationResult(locationRequest: LocationResult?) {
        locationRequest?.run {
            try {
                with(
                    geocoder.getFromLocation(
                        lastLocation.latitude,
                        lastLocation.longitude,
                        1
                    )
                ) {
                    _allowedLocationChannel.offer(allowedLocations.contains(this[0].countryCode))
                }
            } catch (e: IOException) {
            }
        } ?: _allowedLocationChannel.offer(false)
    }

    override fun stopLocationUpdates() {
        fusedLocationProviderClient.removeLocationUpdates(this)
    }

}
