package com.betconnect.app

import android.content.SharedPreferences

const val ONBOARDING_FINISHED = "onboarding_finished"
const val BETCONNECT_PREFERENCES = "internal"
const val BETCONNECT_PREFERENCES_ENCRYPTED = "enc_internal"
const val SHOULD_ASK_FOR_LOCATION_PERMISSION = "SHOULD_ASK_FOR_LOCATION_PERMISSION"

const val INTENT_CUSTOM_PARAM = "customParam1"
const val FINGERPRINT_BLOCKED = "fb_blocked"
const val USE_FINGERPRINT = "param0"

interface IAppPreferences {
    fun setOnboardingCompleted(completed: Boolean)
    fun isOnboardingCompleted(): Boolean
    fun shouldAskForLocationPermission(): Boolean
    fun setShouldAskForLocationPermission(shouldAsk: Boolean)
}

interface FingerprintPreferences {
    fun allowFingerprintLogin(allow: Boolean)
    fun isFingerprintBlocked(): Boolean
    fun setFingerprintBlocked(block: Boolean)
    fun resetAllowFingerprintLogin()
    fun blockFingerprintUse()
    fun isFingerprintLoginAllowedByUser(): Boolean
    fun isFingerprintSettingSet(): Boolean
}

class AppPreferences(private val sharedPreferences: SharedPreferences) : IAppPreferences, FingerprintPreferences {

    override fun allowFingerprintLogin(allow: Boolean) {
        sharedPreferences.edit().putString(USE_FINGERPRINT, if (allow) "1" else "0").apply()
    }

    override fun isFingerprintBlocked(): Boolean {
        return sharedPreferences.getBoolean(FINGERPRINT_BLOCKED, false)
    }

    override fun setFingerprintBlocked(block: Boolean) {
        sharedPreferences.edit().putBoolean(FINGERPRINT_BLOCKED, block).apply()
    }

    override fun resetAllowFingerprintLogin() {
        sharedPreferences.edit().putString(USE_FINGERPRINT, "").apply()
    }

    override fun isFingerprintSettingSet(): Boolean {
        return sharedPreferences.getString(USE_FINGERPRINT, "") != ""
    }

    override fun isFingerprintLoginAllowedByUser(): Boolean {
        return sharedPreferences.getString(USE_FINGERPRINT, "") == "1"
    }

    override fun blockFingerprintUse() {
        sharedPreferences.edit().putString(USE_FINGERPRINT, "NEVER").apply()
    }

    override fun isOnboardingCompleted(): Boolean {
        return sharedPreferences.getBoolean(ONBOARDING_FINISHED, false)
    }

    override fun setOnboardingCompleted(completed: Boolean) {
        sharedPreferences.edit().putBoolean(ONBOARDING_FINISHED, completed).apply()
    }

    override fun shouldAskForLocationPermission(): Boolean {
        return sharedPreferences.getBoolean(SHOULD_ASK_FOR_LOCATION_PERMISSION, true)
    }

    override fun setShouldAskForLocationPermission(shouldAsk: Boolean) {
        sharedPreferences.edit().putBoolean(SHOULD_ASK_FOR_LOCATION_PERMISSION, shouldAsk).apply()
    }

}