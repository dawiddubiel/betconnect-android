package com.betconnect.app.biometricAuth

import android.content.SharedPreferences
import com.betconnect.app.fingerprint.ICredentialsRepo

const val CREDENTIALS = "credentials"

class CredentialsRepoImpl(private val sharedPreferences: SharedPreferences): ICredentialsRepo {

    override fun clearCredentialsData() {
        sharedPreferences.edit().remove(CREDENTIALS).apply()
    }

    override fun setCredentials(credentials: String) {
        sharedPreferences.edit().putString(CREDENTIALS, credentials).apply()
    }

    override fun getCredentials(): String? = sharedPreferences.getString(CREDENTIALS, null)

}