package com.betconnect.app.biometricAuth

import com.fasterxml.jackson.annotation.JsonProperty

data class Credentials(@JsonProperty("login")
                       val login: String,
                       @JsonProperty("password")
                       val password: String)