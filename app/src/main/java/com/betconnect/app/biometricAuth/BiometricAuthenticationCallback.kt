package com.betconnect.app.biometricAuth

import android.os.Build
import androidx.biometric.BiometricPrompt
import com.betconnect.app.FingerprintPreferences
import com.betconnect.app.fingerprint.ICredentialsRepo

class BiometricAuthenticationCallback(
    private val fingerprintPreferences: FingerprintPreferences,
    private val credentialsRepo: ICredentialsRepo,
    inline val onTaskStart: () -> Unit,
    inline val onSuccess: (String?) -> Unit,
    inline val onError: () -> Unit

) : BiometricPrompt.AuthenticationCallback() {

    override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
        super.onAuthenticationSucceeded(result)
        if (isAtLeastApi23()) {
            onTaskStart()
            credentialsRepo.getCredentials()?.let {
                onSuccess(it)
            } ?: run {
                credentialsRepo.clearCredentialsData()
                onError()
            }
        } else {
            fingerprintPreferences.setFingerprintBlocked(true)
            onError()
        }
    }

}

fun isAtLeastApi23() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M