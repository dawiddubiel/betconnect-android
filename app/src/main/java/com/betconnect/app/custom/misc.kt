package com.betconnect.app.custom

import android.graphics.Color
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import android.widget.*
import androidx.constraintlayout.widget.Group
import androidx.core.content.ContextCompat
import com.betconnect.app.R
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import timber.log.Timber


fun TextView.textChangedFlow(defaultEmptyValue: Boolean = false): Flow<String> =
    callbackFlow {
        this@textChangedFlow.apply {
            text.takeIf { it.isNotEmpty() || defaultEmptyValue }?.let {
                offer(it.toString())
            }
            doAfterTextChanged {
                offer(it.toString())
            }
        }
        awaitClose()
    }

inline fun TextView.doAfterTextChanged(crossinline action: (Editable?) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            action(s)
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
    })

}

data class MaxLiabilityPresentation(val value: Int) {
    override fun toString() = "£5-£${value / 100}"
}

inline fun <T : Adapter, reified R> AdapterView<T>.itemSelectionsFlow(): Flow<R?> = callbackFlow {
    (selectedItem as? R)?.let {
        offer(it)
    }

    this@itemSelectionsFlow.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {}

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            offer((selectedItem as? R))
        }
    }
    awaitClose()
}

fun CheckBox.checkChangedFlow(): Flow<Boolean> = callbackFlow {
    this@checkChangedFlow.apply {
        offer(isChecked)
        setOnCheckedChangeListener { _, isChecked ->
            offer((isChecked))
        }
    }
    awaitClose()
}

typealias ClickableTextViewSpans = LinkedHashMap<String, (() -> Unit)?>

fun TextView.setClickableSpans(
    stringId: Int,
    actions: ClickableTextViewSpans,
    highlightColorResId: Int? = null,
    underline: Boolean = true
) =
    SpannableString(context.getString(stringId, *actions.keys.toTypedArray())).apply {
        actions.filter { it.value != null }
            .forEach {
                val clickableSpan = object : ClickableSpan() {
                    override fun onClick(textView: View) {
                        it.value?.invoke()
                    }

                    override fun updateDrawState(ds: TextPaint) {
                        super.updateDrawState(ds.apply {
                            isUnderlineText = underline
                            highlightColorResId?.let {
                                linkColor = ContextCompat.getColor(context, highlightColorResId)
                            } ?: this@setClickableSpans.currentTextColor
                        })
                    }
                }
                setSpan(
                    clickableSpan, indexOf(it.key, ignoreCase = true),
                    indexOf(it.key, ignoreCase = true) + it.key.length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
        text = this
        movementMethod = LinkMovementMethod.getInstance()
        highlightColor = Color.TRANSPARENT
    }



inline fun Group.setAllOnClickListener(crossinline action: () -> Unit) {
    referencedIds.forEach { id ->
        rootView.findViewById<View>(id).setOnClickListener {
            action()
        }
    }
}