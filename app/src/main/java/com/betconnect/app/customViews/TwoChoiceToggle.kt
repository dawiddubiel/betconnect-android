package com.betconnect.app.customViews

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.betconnect.app.R
import com.betconnect.app.extensions.setBackgroundColorInt
import kotlinx.android.synthetic.main.two_choice_toggle_layout.view.*
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.consumeAsFlow

class TwoChoiceToggle @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val channel = BroadcastChannel<String>(Channel.CONFLATED)
    private lateinit var selectedText: String
    private var firstChoice: TextView
    private var secondChoice: TextView
    private val view: View =
        LayoutInflater.from(context).inflate(R.layout.two_choice_toggle_layout, this, true)
    private var txtColorSelected: Int = 0
    private var backgroundColorFirst: Int = 0
    private var txtColorUnselected: Int = 0
    private var backgroundColorSecond: Int = 0

    init {
        firstChoice = view.leftToggleTv
        secondChoice = view.rightToggleTv

        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.TwoChoiceToggle, 0, 0)
            val txtFirst = typedArray.getText(R.styleable.TwoChoiceToggle_text_first).toString()
            val txtSecond = typedArray.getText(R.styleable.TwoChoiceToggle_text_second).toString()
            txtColorSelected =
                typedArray.getColor(R.styleable.TwoChoiceToggle_selected_text_color, Color.WHITE)
            txtColorUnselected =
                typedArray.getColor(R.styleable.TwoChoiceToggle_unselected_text_color, Color.WHITE)
            backgroundColorFirst =
                typedArray.getColor(R.styleable.TwoChoiceToggle_selected_color, Color.WHITE)
            backgroundColorSecond =
                typedArray.getColor(R.styleable.TwoChoiceToggle_unselected_color, Color.WHITE)
            val textSizeSp = typedArray.getFloat(R.styleable.TwoChoiceToggle_textSizeSpp, 16f)

            selectedText = txtFirst

            firstChoice.apply {
                text = txtFirst
                setTextSize(TypedValue.COMPLEX_UNIT_SP, textSizeSp)
                setSelected()
            }
            secondChoice.apply {
                text = txtSecond
                setTextSize(TypedValue.COMPLEX_UNIT_SP, textSizeSp)
                setUnselected()
            }

            channel.offer(selectedText)
            view.setOnClickListener {
                if (selectedText != txtFirst) {
                    selectedText = txtFirst
                    firstChoice.setSelected()
                    secondChoice.setUnselected()
                } else {
                    selectedText = txtSecond
                    secondChoice.setSelected()
                    firstChoice.setUnselected()
                }
                channel.offer(selectedText)
            }
            typedArray.recycle()
        }
    }

    fun setSelection(selection: String) {
        selectedText = selection
        firstChoice.setUnselected()
        secondChoice.setUnselected()

        if (selection.equals(firstChoice.text.toString(), ignoreCase = true)) {
            firstChoice.setSelected()
        } else {
            secondChoice.setSelected()
        }
        channel.offer(selectedText)
    }

    fun getSelectionFlow(): Flow<String> = channel.openSubscription().consumeAsFlow()

    private fun TextView.setSelected() {
        setTextColor(txtColorSelected)
        setBackgroundColorInt(backgroundColorFirst)
    }

    private fun TextView.setUnselected() {
        setTextColor(txtColorUnselected)
        setBackgroundColorInt(backgroundColorSecond)
    }

}
