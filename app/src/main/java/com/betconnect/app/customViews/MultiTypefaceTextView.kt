package com.betconnect.app.customViews

import android.content.Context
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.betconnect.app.R
import io.github.inflationx.calligraphy3.CalligraphyTypefaceSpan
import io.github.inflationx.calligraphy3.TypefaceUtils
import kotlinx.android.synthetic.main.multi_typeface_tv.view.*

class MultiTypefaceTextView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    init {
        LayoutInflater.from(context).inflate(R.layout.multi_typeface_tv, this, true).apply {
            attrs?.let {
                context.obtainStyledAttributes(
                    it,
                    R.styleable.MultiTypefaceTextView, 0, 0
                ).run {
                    mttv.apply {
                        val txtOne = this@run.getText(R.styleable.MultiTypefaceTextView_text_tf_one).toString()
                        val txtTwo = this@run.getText(R.styleable.MultiTypefaceTextView_text_tf_two).toString()
                   //     val txtFontOne = this@run.getText(R.styleable.MultiTypefaceTextView_text_tf_font_one).toString()
                        val textSizeSp = this@run.getFloat(R.styleable.MultiTypefaceTextView_text_tf_size_sp, 16f)
                        setTextSize(TypedValue.COMPLEX_UNIT_SP, textSizeSp)
                        setTextColor(ContextCompat.getColor(context, R.color.logo_color))
                        SpannableStringBuilder().let {
                            it.append(txtOne).append(txtTwo)
                            /*it.setSpan(
                                CalligraphyTypefaceSpan(TypefaceUtils.load(context.assets, txtFontOne)),
                                0,
                                txtOne.length,
                                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                            )*/
                            setText(it, TextView.BufferType.SPANNABLE)
                        }
                    }
                    recycle()
                }

            }
        }
    }
}