package com.betconnect.app.customViews

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.text.InputType
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.betconnect.app.R
import kotlinx.android.synthetic.main.edit_text_with_prefix.view.*

class EditTextWithPrefix @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.edit_text_with_prefix, this, true).apply {
            attrs?.let {
                context.obtainStyledAttributes(it, R.styleable.EditTextWithPrefix, 0, 0).run {
                    prefix.apply {
                        text = this@run.getText(R.styleable.EditTextWithPrefix_prefix).toString()
                        setTextColor(this@run.getColor(R.styleable.EditTextWithPrefix_prefix_text_color, Color.WHITE))
                        background.mutate().setColorFilter(
                            this@run.getColor(
                                R.styleable.EditTextWithPrefix_prefix_background_color,
                                Color.WHITE
                            ), PorterDuff.Mode.SRC_ATOP
                        )
                        setTextSize(
                            TypedValue.COMPLEX_UNIT_SP,
                            this@run.getFloat(R.styleable.EditTextWithPrefix_textSizeSppp, 16f)
                        )

                    }
                    editText.apply {
                        hint = this@run.getText(R.styleable.EditTextWithPrefix_hint).toString()
                        setTextColor(this@run.getColor(R.styleable.EditTextWithPrefix_et_text_color, Color.WHITE))
                        setTextSize(
                            TypedValue.COMPLEX_UNIT_SP,
                            this@run.getFloat(R.styleable.EditTextWithPrefix_textSizeSppp, 16f)
                        )
                        inputType = InputType.TYPE_CLASS_NUMBER
                    }

                    recycle()
                }
            }
        }
    }
}
