package com.betconnect.app.customViews

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.betconnect.app.R
import kotlinx.android.synthetic.main.icon_with_text_layout.view.*

class IconWithText @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    init {
        LayoutInflater.from(context).inflate(R.layout.icon_with_text_layout, this, true).apply {
            attrs?.let {
                context.obtainStyledAttributes(it, R.styleable.IconWithText, 0,0).run {
                    text.apply {
                        text = this@run.getText(R.styleable.IconWithText_text)
                        setTextSize(TypedValue.COMPLEX_UNIT_SP, this@run.getFloat(R.styleable.IconWithText_textSizeSp, 16f))
                        setTextColor(this@run.getColor(R.styleable.IconWithText_textColor, Color.WHITE))
                    }
                    icon.apply {
                        setColorFilter(this@run.getColor(R.styleable.IconWithText_iconColor, Color.WHITE))
                        setImageResource(this@run.getResourceId(R.styleable.IconWithText_icon, R.drawable.ic_connect))
                    }
                    recycle()
                }
            }
        }
    }
}
