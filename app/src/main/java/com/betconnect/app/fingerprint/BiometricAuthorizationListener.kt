package com.betconnect.app.fingerprint

interface BiometricAuthorizationListener {
    fun onCredentialsDecrypted(login: String, password: String)
    fun onAuthorizationError()
}