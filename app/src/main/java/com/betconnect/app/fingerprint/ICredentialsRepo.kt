package com.betconnect.app.fingerprint

interface ICredentialsRepo {
    fun clearCredentialsData()
    fun setCredentials(credentials: String)
    fun getCredentials() : String?
}