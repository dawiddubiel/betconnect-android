package com.betconnect.app

interface IBaseController {
    fun showProgress(show: Boolean)
    fun showError(errorMessage: String)
}
