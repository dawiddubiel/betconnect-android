package com.betconnect.networking


sealed class Result<T> {
    data class Success<T>(val value: T) : Result<T>()
    data class Error<Nothing>(val value: ErrorType = ErrorType.DEFAULT()) : Result<Nothing>()
}

sealed class ErrorType(open val message: String = "") {
    class DEFAULT(override val message: String = "") : ErrorType()
    class NETWORK(override val message: String = "") : ErrorType()
    class UNAUTHORIZED(override val message: String = "") : ErrorType()
    class FORBIDDEN(override val message: String = "") : ErrorType()
    class NOT_FOUND(override val message: String = "") : ErrorType()
    class CONFLICT(override val message: String = "") : ErrorType()
    class GONE(override val message: String = "") : ErrorType()
    class FATAL(override val message: String = "") : ErrorType()
}