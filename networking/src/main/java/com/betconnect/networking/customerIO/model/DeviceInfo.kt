package com.betconnect.networking.customerIO.model

import com.fasterxml.jackson.annotation.JsonProperty

data class DeviceInfo(
    @get:JsonProperty(value = "id") val id: String,
    @get:JsonProperty(value = "platform") val platform: String,
    @get:JsonProperty(value = "last_used") val last_used: Long
)

data class Device(@get:JsonProperty(value = "device") val device: DeviceInfo)