package com.betconnect.networking.customerIO

import com.betconnect.networking.customerIO.model.Device
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.PUT
import retrofit2.http.Path

const val ANDROID_PLATFORM_NAME = "android"

interface CustomerIoApi {

    @PUT("customers/{handle}/devices")
    suspend fun pushDeviceInfo(@Path("handle") handle: String, @Body loginData: Device): Response<Unit>

}