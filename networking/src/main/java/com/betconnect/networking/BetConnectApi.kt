package com.betconnect.networking

import com.betconnect.networking.apiModel.LoginData
import com.betconnect.networking.apiModel.ResetPasswordData
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface BetConnectApi {

    @POST("mobile/login")
    suspend fun login(@Body loginData: LoginData): Response<ResponseBody>

    @POST("mobile/forgot-password")
    suspend fun resetPassword(@Body resetPasswordData: ResetPasswordData): Response<Unit>

}
