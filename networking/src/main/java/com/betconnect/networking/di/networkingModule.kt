package com.betconnect.networking.di

import android.webkit.CookieManager
import com.betconnect.networking.*
import com.betconnect.networking.address.LoqateApi
import com.betconnect.networking.customerIO.CustomerIoApi
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

const val ADDRESSY_API_URL = "https://api.addressy.com/"
const val BASE_URL = "https://app.betconnect.com"
const val LoggingLevel = "LoggingLevel"

val networkingModule = module {

    single(named(LoggingLevel)) { if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE }

    single {
        OkHttpClient.Builder()
            .readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor().apply { level = get(named(LoggingLevel)) })
            .build()
    }


    single<CustomerIoApi> {
        Retrofit.Builder()
            .baseUrl(BuildConfig.CustomerIoBaseUrl)
            .addConverterFactory(JacksonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(
                OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor().apply {
                        level = get(named(LoggingLevel))
                    })
                    .addInterceptor(
                        BasicAuthInterceptor(
                            BuildConfig.CustomerIoUsername,
                            BuildConfig.CustomerIoPassword
                        )
                    )
                    .addInterceptor { chain ->
                        val request = chain.request().newBuilder().addHeader(
                            "Authorization",
                            "Basic YjQwYjViNzBjOGQ2ZjgxNDMyMzI6ZjI2ZTk2ZjA3ODk4NWI5ZTk1M2Y="
                        ).build()
                        chain.proceed(request)
                    }
                    .build()
            )
            .build()
            .create(CustomerIoApi::class.java)
    }


    single<BetConnectApiTransit> {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(
                OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor().apply {
                        level = get(named(LoggingLevel))
                    }).build()
            )
            .build()
            .create(BetConnectApiTransit::class.java)
    }

    single<BetConnectApi> {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(JacksonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(
                OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor().apply {
                        level = get(named(LoggingLevel))
                    }).build()
            )
            .build()
            .create(BetConnectApi::class.java)
    }

    single<LoqateApi> {
        Retrofit.Builder()
            .baseUrl(ADDRESSY_API_URL)
            .addConverterFactory(JacksonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(
                OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor().apply {
                        level = get(named(LoggingLevel))
                    }).build()
            )
            .build()
            .create(LoqateApi::class.java)
    }

    single {
        RemoteDataSource(
            jsonBetConnectApi = get(),
            transitBetConnectApi = get(),
            loqateApi = get(),
            customerIoApi = get()
        )
    }

    single { CookieManager.getInstance() }

}