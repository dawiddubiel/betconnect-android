package com.betconnect.networking.address

import com.betconnect.networking.BuildConfig
import com.betconnect.networking.apiModel.AddressDetailsResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface LoqateApi {

    @GET("Capture/Interactive/Find/v1.10/json3.ws?")
    suspend fun getLocations(
        @Query(value = "Key") key: String = BuildConfig.LoqateApiKey,
        @Query(value = "Text") text: String,
        @Query(value = "IsMiddleware") isMiddleware: Boolean = true,
        @Query(value = "Countries") countries: String = "GB",
        @Query(value = "Limit") limit: Int = 100,
        @Query(value = "Container") container: String? = ""): Response<AddressResultResponse>

    @GET("Capture/Interactive/Retrieve/v1.00/json3.ws?")
    suspend fun retrieveLocations(
        @Query(value = "Key") key: String = BuildConfig.LoqateApiKey,
        @Query(value = "Id") id: String): Response<AddressDetailsResponse>

}