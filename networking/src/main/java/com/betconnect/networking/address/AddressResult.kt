package com.betconnect.networking.address

import com.fasterxml.jackson.annotation.JsonProperty

const val RESULT_TYPE_ADDRESS = "Address"

data class AddressResultResponse(@JsonProperty("Items") val items: List<AddressResult>)

data class AddressResult(
    @JsonProperty("Id") val id: String?,
    @JsonProperty("Type") val type: String?,
    @JsonProperty("Text") val text: String?,
    @JsonProperty("Highlight") val highlight: String?,
    @JsonProperty("Description") val description: String?
) {

    override fun toString() = "${this.text}, ${this.highlight}, ${this.description}"

}
