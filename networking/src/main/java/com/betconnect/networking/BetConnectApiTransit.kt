package com.betconnect.networking

import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface BetConnectApiTransit {

    @Headers("Content-Type: application/transit+json")
    @POST("joinin")
    suspend fun register(@Body registrationData: String): Response<ResponseBody>

}