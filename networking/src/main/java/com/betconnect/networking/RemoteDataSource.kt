package com.betconnect.networking

import com.betconnect.networking.address.LoqateApi
import com.betconnect.networking.apiModel.LoginData
import com.betconnect.networking.apiModel.LoginResponse
import com.betconnect.networking.apiModel.ResetPasswordData
import com.betconnect.networking.apiModel.registrationData.RegistrationData
import com.betconnect.networking.apiModel.registrationData.asBody
import com.betconnect.networking.customerIO.CustomerIoApi
import com.betconnect.networking.customerIO.model.Device
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.coroutines.suspendCancellableCoroutine
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response
import java.io.IOException
import kotlin.coroutines.resume

const val LOGIN_FAILED = "login_failed"
const val SIGNUP_FAILED = "signup_failed"
const val LOCATION_FAILED = "location_failed"
const val HEADER_COOKIE_KEY = "Set-Cookie"

class RemoteDataSource(
    private val jsonBetConnectApi: BetConnectApi,
    private val transitBetConnectApi: BetConnectApiTransit,
    private val loqateApi: LoqateApi,
    private val customerIoApi: CustomerIoApi
) {

    suspend fun login(loginData: LoginData) =
        safeCall(call = { jsonBetConnectApi.login(loginData) }, parserMethod = { response ->
            if (response.isSuccessful) {
                response.body()?.let { responseBody ->
                    JSONObject(responseBody.string()).let { json ->
                        LoginResponse(
                            json.getString("redirect-page"),
                            json.getString("user-role"),
                            response.headers().get(
                                HEADER_COOKIE_KEY
                            )
                        )
                    }
                } ?: throw IOException(LOGIN_FAILED)

            } else {
                response.headers().get("Location")?.let {
                    throw IOException(LOCATION_FAILED)
                } ?: throw IOException(LOGIN_FAILED)
            }
        })

    suspend fun signUp(registrationData: RegistrationData) =
        safeCall(call = {
            transitBetConnectApi.register(registrationData.asBody())
        }, parserMethod = { response ->
            if (response.isSuccessful) {
                response.headers()[HEADER_COOKIE_KEY]
            } else {
                response.errorBody()?.string()?.let {
                    if (it.contains("http://www.betconnect.com/punter/maintenance-assets/itsnotyou.php")) throw IOException(
                        LOCATION_FAILED
                    )
                    it.split(",").let {
                        throw IOException(it[it.toMutableList().indexOfFirst { asd -> asd.contains(":desc") } + 1].replace(
                            "\"",
                            ""
                        ))
                    }
                } ?: throw IOException("Unknown error")
            }
        })

    suspend fun forgotPassword(resetPasswordData: ResetPasswordData) =
        safeCall(
            call = { jsonBetConnectApi.resetPassword(resetPasswordData).successOrThrow() },
            parserMethod = { it })

    suspend fun getLocation(input: String, container: String?) = safeCall(
        call = {
            loqateApi.getLocations(
                text = input,
                container = container
            ).bodyOrThrow()
        }, parserMethod = { it }
    )

    suspend fun retrieveLocation(id: String) = safeCall(call = {
        loqateApi.retrieveLocations(
            id = id
        ).bodyOrThrow()
    }, parserMethod = { it })

    suspend fun pushDeviceInfo(handle: String, device: Device) = safeCall(
        call = { customerIoApi.pushDeviceInfo(handle, device).bodyOrThrow() },
        parserMethod = { it }
    )

    suspend fun getFirebaseId() = suspendCancellableCoroutine<String?> { continuation ->
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener { task ->
                task.takeIf { it.isSuccessful }
                    ?.result
                    ?.let { continuation.resume(it.token) }
                    ?: continuation.resume(null)
            }
    }

    private inline fun <T, R> RemoteDataSource.safeCall(
        call: RemoteDataSource.() -> T,
        parserMethod: (T) -> R,
        onSuccess: (T) -> Unit = {},
        onError: () -> Unit = {}
    ): Result<R> =
        try {
            Result.Success(parserMethod(call().also { onSuccess(it) }))
        } catch (e: IOException) {
            onError()
            timber.log.Timber.e(e)
            Result.Error(
                when (e) {
                    is UnauthorizedException -> ErrorType.UNAUTHORIZED(e.message)
                    is ForbiddenException -> ErrorType.FORBIDDEN(e.message)
                    is NotFoundException -> ErrorType.NOT_FOUND(e.message)
                    is ConflictException -> ErrorType.CONFLICT(e.message)
                    is GoneException -> ErrorType.GONE(e.message)
                    is ServerSideException -> ErrorType.DEFAULT(e.message)
                    else -> ErrorType.DEFAULT(e.message.orEmpty())
                }
            )
        }

    private fun Response<Unit>.successOrThrow() =
        takeIf { it.isSuccessful }?.let { Unit } ?: throwExceptionFromCode()

    private fun <T> Response<T>.bodyOrThrow() =
        takeIf { it.isSuccessful }?.body() ?: throwExceptionFromCode()

    private fun Response<ResponseBody>.bytesOrThrow() =
        takeIf { it.isSuccessful }?.body()?.bytes() ?: throwExceptionFromCode()

    private fun <T> Response<T>.throwExceptionFromCode(): Nothing = throw when (code()) {
        400 -> DefaultException(errorBody()?.string().orEmpty())
        401 -> UnauthorizedException(errorBody()?.string().orEmpty())
        403 -> ForbiddenException(errorBody()?.string().orEmpty())
        404 -> NotFoundException(errorBody()?.string().orEmpty())
        409 -> ConflictException(errorBody()?.string().orEmpty())
        410 -> GoneException(errorBody()?.string().orEmpty())
        500 -> ServerSideException(errorBody()?.string().orEmpty())
        else -> DefaultException(errorBody()?.string().orEmpty())
    }

}

sealed class RemoteException(override val message: String = "") : IOException()

class DefaultException(override val message: String = "") : RemoteException(message)
class UnauthorizedException(override val message: String = "") : RemoteException(message)
class ForbiddenException(override val message: String = "") : RemoteException(message)
class NotFoundException(override val message: String = "") : RemoteException(message)
class ConflictException(override val message: String = "") : RemoteException(message)
class GoneException(override val message: String = "") : RemoteException(message)
class ServerSideException(override val message: String = "") : RemoteException(message)