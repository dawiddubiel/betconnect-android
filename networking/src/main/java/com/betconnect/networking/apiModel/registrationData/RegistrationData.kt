package com.betconnect.networking.apiModel.registrationData

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

const val REGISTRATION_DATA = "RegistrationData"

@Parcelize
data class RegistrationData(
    var title: String? = null,
    var lastname: String? = null,
    var password: String? = null,
    var passwordConfirm: String? = null,
    var firstname: String? = null,
    var maxLiabilityInCents: Int? = null,
    var role: UserRole? = null,
    var email: String? = null,
    var termsAccepted: Boolean? = null,
    var depositLimitDuration: EDepositLimitDuration? = null,
    var userAddress: UserAddress = UserAddress(),
    var handle: String? = null,
    var contactNumber: String? = null,
    var marketingTermsAccepted: Boolean? = null,
    var deviceToken: String? = null,
    var depositLimitAmount: Int? = null,
    var sportsPreferences: List<SportsPreferences>? = null,
    var birthdate: BirthDate? = null
) : Parcelable


@Parcelize
data class BirthDate(
    var yearOfBirth: Int,
    var monthOfBirth: Int,
    var dayOfBirth: Int
) : Parcelable {
    constructor(dateFormatted: String) : this(
        dateFormatted.split("-")[2].toInt(),
        dateFormatted.split("-")[1].toInt(),
        dateFormatted.split("-")[0].toInt()
    )
}


fun RegistrationData.asBody(): String {
    return "[\"^ \",\n" +
            "    \"~:user/device-token\",\"$deviceToken\",\n" +
            "    \"~:user.dob/year\",${birthdate?.yearOfBirth},\n" +
            "    \"~:user.dob/month\",${(birthdate?.monthOfBirth!! + 1)},\n" +
            "    \"~:user.dob/day\",${birthdate?.dayOfBirth},\n" +
            "    \"~:user/lastname\",\"$lastname\",\n" +
            "    \"~:user/password\",\"$password\",\n" +

            "    \"~:user/firstname\",\"$firstname\",\n" +
            "    \"~:marketing-terms/accepted?\",$marketingTermsAccepted,\n" +
            "    \"~:user/contact-number\",\"$contactNumber\",\n" +
            "    \"~:user/title\",\"$title\",\n" +
            if (role?.value == "punter") {
                "    \"~:user/max-liability-in-cents\",$maxLiabilityInCents,\n"
            } else {
                ""
            } +
            "    \"~:user/roles\",[\"~#set\",[\"~:user.role/${role?.value}\"]],\n" +
            "    \"~:terms/accepted?\",$termsAccepted,\n" +
            "    \"~:user/email\",\"$email\",\n" +
            (depositLimitAmount?.let { "    \"~:deposit-limit/amount\",$it,\n" }
                ?: "    \"~:deposit-limit/amount\",0,\n") +
            depositLimitDuration?.let { "    \"~:deposit-limit/duration\"," + "\"${it.getTransitValue()}\",\n" } +
            "    \"~:user/address\",\n" +
            "        [\"^ \",\n" +
            "            \"~:address/street\",\"${userAddress.street}\",\n" +
            "            \"~:address/city\",\"${userAddress.city}\",\n" +
            "            \"~:address/sub-city\",\"${userAddress.subCity}\",\n" +
            "            \"~:address/postcode\",\"${userAddress.postalCode}\",\n" +
            "            \"~:address/building\",\"${userAddress.building}\"\n" +
            "        ],\n" +
            "    \"~:user/handle\",\"${this.handle}\",\n" +
            if (role?.value == "punter") getTransitPreferences(sportsPreferences!!) else {
                ""
            } +
            "    \"~:user/password-confirm\",\"$passwordConfirm\"\n" +
            "]"
}

enum class EDepositLimitDuration(val value: String) {
    DAY("24 Hours"), WEEK("7 Days"), MONTH("1 Month"), UNLIMITED("No Limit");

    override fun toString() = value

    companion object {
        fun valOf(value: String): EDepositLimitDuration? = values()
            .find { it.value == value }
    }

    fun getTransitValue(): String {
        return when (this) {
            DAY -> "~:limit-duration/day"
            WEEK -> "~:limit-duration/week"
            MONTH -> "~:limit-duration/month"
            UNLIMITED -> "~:limit-duration/unlimited"
        }
    }

}

@Parcelize
enum class UserRole(val value: String) : Parcelable {
    Punter("punter"), Pro("pro");

    companion object {
        fun valOf(value: String): UserRole? = values()
            .find { it.value == value.toLowerCase() }
    }
}

fun getTransitPreferences(preferences: List<SportsPreferences>) = if (preferences.isNotEmpty()) {
    ("    \"~:user/sports-preferences\"," + "[" + preferences.toTransitString() + "],\n")
} else {
    ""
}

enum class SportsPreferences(val displayName: String, val dbId: Long, val sportType: String) {
    Football("Football", 10, "football"),
    HorseRacing("Horse\nRacing", 14, "horse-racing"),
    RugbyLeague("Rugby\nLeague", 73743, "rugby-league"),
    RugbyUnion("Rugby\nUnion", 73744, "rugby-union"),
    Basketball("Basketball", 4, "basketball"),
    Darts("Darts", 8, "darts"),
    Golf("Golf", 12, "golf"),
    Tennis("Tennis", 24, "tennis"),
    Cricket("Cricket", 6, "cricket"),
    AmericanFootball("American\nFootball", 17, "american-football"),
    Snooker("Snooker", 22, "snooker"),
    Boxing("Boxing", 5, "boxing"),
    Baseball("Baseball", 3, "baseball")
}

fun SportsPreferences.getTransitValue(): String {
    return "[\"^ \", \"~:db/ident\", \"~:sport-type/${this.sportType}\", \"~:betgenius.sport/id\", ${this.dbId}, \"~:betgenius.sport/name\", \"${this.name}\"]"
}

fun List<SportsPreferences>.toTransitString(): String {
    val stringBuilder = StringBuilder()
    forEach {
        stringBuilder.append(",")
        stringBuilder.append(it.getTransitValue())
    }
    return if (stringBuilder.toString().length > 1) stringBuilder.toString().substring(1) else stringBuilder.toString()
}

@Parcelize
data class SportPreferencesListItem(val option: SportsPreferences, var selected: Boolean) :
    Parcelable
