package com.betconnect.networking.apiModel

import com.fasterxml.jackson.annotation.JsonProperty


data class LoginData(@get:JsonProperty(value = "user/handle") val email: String,
                     @get:JsonProperty(value = "user/password") val password: String,
                     @get:JsonProperty(value = "user/device-token") val deviceToken: String)