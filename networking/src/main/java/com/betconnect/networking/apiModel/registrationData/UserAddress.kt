package com.betconnect.networking.apiModel.registrationData

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserAddress(
    var street: String? = null,
    var city: String? = null,
    var subCity: String? = null,
    var postalCode: String? = null,
    var building: String? = null,
    var premise: String? = null
) : Parcelable

fun UserAddress.getStringValue(): String {
    val stringBuilder = StringBuilder()
    if (!building.isNullOrBlank()) {
        stringBuilder.append("${this.building}, ")
    }
    if (!street.isNullOrBlank()) {
        stringBuilder.append("${this.street}, ")
    }
    if (!premise.isNullOrBlank()) {
        stringBuilder.append("${this.premise}, ")
    }
    if (!city.isNullOrBlank()) {
        stringBuilder.append("${this.city}, ")
    }
    if (!postalCode.isNullOrBlank()) {
        stringBuilder.append("${this.postalCode}")
    }
    return stringBuilder.toString()
}