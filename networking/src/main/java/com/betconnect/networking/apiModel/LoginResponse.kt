package com.betconnect.networking.apiModel

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty

data class LoginResponse(
    @get: JsonProperty(value = "redirect-page") val page: String,
    @get:JsonProperty(value = "user-role") val userRole: String,
    @JsonIgnore var cookie: String?
)