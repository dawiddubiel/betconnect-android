package com.betconnect.networking.apiModel

import com.fasterxml.jackson.annotation.JsonProperty

data class ResetPasswordData(@get:JsonProperty(value = "user/email") val email: String)